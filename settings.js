var os = require('os');
var ifaces = os.networkInterfaces();
var WEBSOCKET_HOST = 'localhost'

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (iface.address.indexOf('192') >= 0) {
      // this single interface has multiple ipv4 addresses
      WEBSOCKET_HOST = iface.address
    }
    ++alias;
  });
});


module.exports = {
    WEBPACK_HOST: WEBSOCKET_HOST,
    WEBSOCKET_ADDRESS: WEBSOCKET_HOST + ':8887'
}
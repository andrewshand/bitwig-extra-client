import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { configureStore } from './store';
import { Tablet } from './components/Tablet';
// import 'fastclick'

const store = configureStore();
const history = createBrowserHistory();

ReactDOM.render(
  <Tablet />,
  document.getElementById('app')
);

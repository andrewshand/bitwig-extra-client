import * as _ from 'underscore'
var Base64 = require('js-base64').Base64;
import { compose, watch, Watchable, provide } from '@webantic/compose'
import { resolveSet } from '@webantic/util/object'

export namespace Bitwig {
    export interface EQBand {
        freq: number,
        q: number,
        amp: number,
        on: boolean,
        type: number
    }
    export interface EQPacket {
        bands: EQBand[]
    }
    export interface TrackPacket {
        volume: number,
        volumeString: string,
        pan: number,
        name: string,
        color: string,
        type: 'standard' | 'group',
        index: number,
        solo: boolean,
        mute: boolean,
        eq: EQPacket
    }
    export interface SavedData {
        tags: TagData[]
    }
    export interface TagData {
        tag: string,
        start: string
    }
}

const lateMessages: any[] = []
const onMessage: Function[] = []
export function onMessageReceived(callback) {
    onMessage.push(callback)
}

declare const TextEncoder: any;
declare const TextDecoder: any;

let changedTransportCountWait = 0


export function post(resource: any, modifier?: any) {
    if (resource.indexOf('transport') > -1) {
        changedTransportCountWait = 100
    }
    const toSend = `<header>${JSON.stringify({ resource, modifier })}<footer>`
    function sendPart(toSend) {
        var arr = new ArrayBuffer(4); // an Int32 takes 4 bytes
        var view = new DataView(arr);
        const length = toSend.length //new TextEncoder('utf-8').encode(toSend).length
        view.setUint32(0, length, false); // byteOffset = 0; litteEndian = false
        const prependedSizeByteString = new TextDecoder('utf-8').decode(new Uint32Array(arr))
        // console.log(toSend)
        ws.send(Base64.encode(prependedSizeByteString + toSend))
    }
    var remaining = toSend
    while (remaining.length > 0) {
        sendPart(remaining.substr(0, 50))
        remaining = remaining.substr(50)
    }
}


export function getLatestLevels() {
    return latestLevels
}
let latestLevels: number[] = new Array()
latestLevels.fill(0)
const levelCallbacks: { [id: string]: Function } = {};
let nextId = 0
export function onLevels(callback: Function) {
    const id = nextId++
    levelCallbacks[id] = callback
    return {
        off() {
            delete levelCallbacks[id]
        }
    }
}
const ws = new WebSocket(`ws://${WEBSOCKET_ADDRESS}`, "base64");
ws.onmessage = (event) => {
    const data = atob(event.data);
    try {
        const separator = "<MESSAGEHEADERFUCKYOU>"
        const messages = data.split(separator)
        messages.forEach(function (message) {
            message = message.trim()
            if (message.length === 0) {
                return
            }
            const parsed = JSON.parse(message);
            //console.log(parsed);
            onMessage.forEach(function (callback) {
                callback(parsed)
            })

            if (parsed.type === 'levels') {
                latestLevels = parsed.data
                _.values(levelCallbacks).forEach(function (callback) {
                    callback(parsed.data)
                })
            }
        })

    } catch (e) {
        console.error(e)
        console.log(data)
    }

}

interface BitwigTrack {
    volume: number,
    volumeString: string,
    pan: number,
    name: string,
    color: string,
    children?: BitwigTrack[]
    type: 'standard' | 'group',
    index: number,
    solo: boolean,
    mute: boolean,
    eq: Bitwig.EQPacket
}
interface BitwigTransport {
    position: number,
    playing: boolean,
    looping: boolean
}
export interface BitwigState {
    tracks: BitwigTrack[],
    allTracks: BitwigTrack[],
    tracksByName: { [key: string]: BitwigTrack }
    transport: BitwigTransport
    recording: boolean,
    metronome: boolean
}
const state: Watchable<BitwigState> = new Watchable()
export const setState = (modifier) => {
    const currentState = state.get()
    Object.keys(modifier).forEach(mod => {
        resolveSet(currentState, mod, modifier[mod], {
            makeArrays: true,
            makeObjects: true
        })
    })
    state.set(currentState)
}
state.set({
    tracks: [],
    allTracks: [],
    tracksByName: {},
    transport: {
        position: 0,
        playing: false,
        looping: false
    },
    recording: false,
    metronome: false
})
let connected = false
let playingNotes = []
const transportUpdater = (modifier) => {
    const timeSig = modifier.timeSignature
    const topLevel = ['recording', 'metronome']
    const stateUpdate = {
        transport: modifier,
        ..._.pick(modifier, topLevel)
    }
    setState(stateUpdate)
}
const debouncedTransportUpdate = _.throttle(transportUpdater, 2000)
let trackEls = {}
let levelMeters: any = []
setInterval(() => {
    // collect level meters
    levelMeters = Array.from(document.querySelectorAll('.levelMeter')).map((el: any) => {
        el.index = el.className.split(' ').map(name => {
            if (name.indexOf('track/') === 0) {
                return name.substr('track/'.length)
            }
            return ''
        }).join('').trim()
        return el
    })

    document.querySelectorAll('.track').forEach(el => {
        el.className.split(' ').forEach(name => {
            if (name.indexOf('track/') === 0) {
                trackEls[name.substr('track/'.length)] = el
            }
        })
    })

}, 500)

onMessageReceived((message) => {
    if (!connected) {
        connected = true
        // post({ 'data': 'currentProject' })
    }
    if (message.type === 'tracks') {
        // update all tracks
        
        // find tracks that are group tracks
        const makeChildren = tracks => {
            let newTracks = []
            for (const track of tracks) {
                track.children = []
                if (track.name.indexOf('Master') >= 0) {
                    // see if we have a track that has its name minus the master bit, if so - that's the start of the group track
                    const nameWithoutMaster = (track.name.substr(0, track.name.length - 'Master'.length)).trim()
                    const groupStart = newTracks
                        .slice()
                        .reverse()
                        .findIndex(potentialStart => potentialStart.name.trim() === nameWithoutMaster)
                    if (groupStart >= 0) {
                        const children = newTracks.splice(newTracks.length - groupStart, groupStart)
                        // group start relative to end has moved as we took away children
                        const firstGroupTrack = newTracks[newTracks.length - 1]
                        firstGroupTrack.children = children
                        continue
                    }
                }
                newTracks.push(track)
            }
            return newTracks
        }
        setState({
            tracks: makeChildren(message.data),
            allTracks: message.data,
            tracksByName: _.indexBy(message.data, 'name')
        })
    }
    if (message.type === 'notes') {
        message.data.forEach((notes, index) => {
            const el = trackEls[index]
            if (!el) {
                return
            }
            if (!playingNotes[index]) {
                playingNotes[index] =
                    {
                        current: new Set(),
                        records: []
                    }
            }
            const reff = playingNotes[index]

            if (index === 0) {
            }

            for (const note of notes) {
                if (!reff.current.has(note)) {
                    reff.current.add(note)
                    reff.records = reff.records.slice(-10).concat({
                        on: new Date().getTime(),
                        note
                    })
                }
            }
            Array.from(reff.current.values()).forEach(current => {
                // if we hold a note as current which is no longer current
                if (notes.indexOf(current) === -1) {
                    // delete it from our refs
                    reff.current.delete(current)

                    // if the note on still exists (might be too old in some cases)
                    const index = reff.records.findIndex(record => {
                        return !record.off && record.note === current
                    })
                    // record an off event
                    if (index > -1) {
                        reff.records[index].off = new Date().getTime()
                    }
                }
            })

            el.className = (el.className.replace('-playing', '') + (notes.length === 0 ? '' : ' -playing')).trim()
        })
    }
    if (message.type === 'levels') {
        const latestLevels = message.data

    }
    if ('resource' in message) {
        if (message.resource === 'transport') {
            changedTransportCountWait--
            const update = message.modifier as BitwigTransport
            const timeline = document.getElementById('timeline')
            const marker = document.getElementById('marker')
            const goLeft = document.getElementById('goLeft')
            const goRight = document.getElementById('goRight')
            const beat = timeline.querySelector('.beat') as any
            if ('position' in update) {
                marker.setAttribute('style', `transform: translateX(${(update.position / 4) * beat.offsetWidth}px);`)
            }
            const rect = marker.getBoundingClientRect()
            goLeft.setAttribute('style', '')
            goRight.setAttribute('style', '')
            if (rect.left >= window.innerWidth) {
                goRight.setAttribute('style', 'opacity: 1')
            } else if (rect.right < 0) {
                goLeft.setAttribute('style', 'opacity: 1')
            }
            if (changedTransportCountWait > 0) {
                transportUpdater(message.modifier)
            } else {
                debouncedTransportUpdate(message.modifier)
            }
        }
        // if (message.resource === 'state') {
        //     setState(message.modifier)
        //     return
        // }
        if (typeof message.resource !== 'string') {
            if ('track' in message.resource) {
                const { name } = message.resource.track
                const trackIndex = state.get().allTracks.findIndex(track => track.name === name)

                if (trackIndex > -1) {
                    Object.assign(state.get().allTracks[trackIndex], message.modifier)
                }

                state.notifyWatchers()
            }
        }
    }
})

const composer = compose(
    watch(state),
    provide(props => {
        return {
            bitwig: state.get()
        }
    })
)
export const withBitwigState = (Component) => composer(Component)

const canvas = document.createElement('canvas')
document.body.appendChild(canvas)
canvas.setAttribute('style', `
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    pointer-events: none;
`)
const setCanvasWidth = () => {
    canvas.setAttribute('width', window.innerWidth)
    canvas.setAttribute('height', window.innerHeight)
}
const timeRange = (1000 * 1.25)

window.onresize = setCanvasWidth

const drawNotes = () => {
    const levels = latestLevels
    for (const el of levelMeters) {
        const index = (el as any).index
        const percent = (100 - (levels[index] / 127 * 100)) + '%';

        el.setAttribute('style', `
            clip-path: inset(${percent} 0 0 0);
        `)
    }
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight)
    let index = -1
    for (const notes of playingNotes) {
        index++
        const el = trackEls[index]
        if (!el || !notes) {
            continue
        }

        const records = notes.records
        if (records.length === 0) {
            continue
        }

        const rect = el.getBoundingClientRect()
        if (rect.right < 0 || rect.left > window.innerWidth) {
            // skip offscreen tracks
            continue
        }

        const noteVlaues = records.map(note => note.note)
        const minNote = Math.min.apply([], noteVlaues)
        const maxNote = Math.max.apply([], noteVlaues)
        const noteRange = maxNote - minNote
        const backDateUntil = new Date().getTime() - timeRange
        for (const { note, on, off } of records) {
            const percentThroughTime = (on - backDateUntil) / timeRange
            const offPercentThroughTime = (off - backDateUntil) / timeRange
            const x = Math.max(percentThroughTime * rect.width, 0)
            const end = off ? Math.max(offPercentThroughTime * rect.width, 0) : rect.width
            if (end < 0) {
                continue
            }
            const width = end - x
            const inRange = note === minNote ? 1 : (note - minNote) / noteRange
            const height = rect.height / (noteRange + 1)
            const y = rect.height - ((1 - inRange) * rect.height) - height
            ctx.fillStyle = 'rgba(255, 255, 255,0.3)';
            ctx.fillRect(
                rect.left + x,
                rect.top + y,
                width,
                height
            )
        }
    }
    window.requestAnimationFrame(drawNotes)
}
window.requestAnimationFrame(drawNotes)

setTimeout(setCanvasWidth, 1000)

import * as icons from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { compose, provide, watch, Watchable } from '@webantic/compose';
import { resolve } from '@webantic/util/object';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import * as React from 'react';
import styled, { injectGlobal } from 'styled-components';
import '../../containers/App/style.css';
import { BitwigState, post, withBitwigState } from '../Bitwig';
import { Controls } from './Controls';

const style = { float: 'left', width: 160, height: 400, marginBottom: 160, marginLeft: 50 };

const touchedTrack = new Watchable<Track>()
export namespace Tablet {
    export interface Props {
        bitwig: BitwigState
    }

    export interface State {

    }
}
injectGlobal`
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');
    font-family: 'Source Sans Pro';
    font-weight: 400;
    * {
        user-select: none;
    }
`
const VerticalButtons = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
flex-shrink: 0;
flex-grow: 0;
`
const SideButtonSize = '2em'

const Button = styled.div`
    width: ${SideButtonSize};
    height: ${SideButtonSize};
    font-size: 1.8em;
    margin: .2em;
    display: flex;
    background: linear-gradient(#808080, #646464);
    align-items: center;
    justify-content: center;
    color: white;
    ${(props: any) => props.active ? `
        background: ${props.mainColor || '#F38011'};
        color: black;
    `: ``}
    &:active {
        box-shadow: 0 0 4rem rgba(0, 0, 0, 0.2) inset;
    }
    box-shadow: 0.05em 0.1em 0.5em rgb(0, 0, 0, 0.4);

`
const ReloadButton = styled.div`
    position: fixed;
    bottom: 1em;
    left: 1em;
    height: 2em;
    width: 2em;
    border-radius: 1000px;
    background: grey;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`
const HorizontalWrap = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    display: flex;
    overflow: hidden;
`
const Wrap = styled.div`
    background :#353535;
    flex-grow: 1;
    color: white;
    display: flex;
    flex-direction: column;
    overflow: hidden;
`
const Spacer = styled.div`

    flex-grow: 1;
`
const levelMeterWidth = '0.8em';
const TrackWrap = styled.div`
    @keyframes play {
        0% {
            transform: scale(1);

        }
        10% {
            transform: scale(1.02);

        }
        100% {
            transform: scale(1);

        }
    }
    &.-playing {
        filter: brightness(130%);
        animation-name: play;
        animation-duration: .2s;
        animation-timing-function: linear;
        animation-fill-mode: forwards;
    }
    &:active {
        transform: scale(.94);
    }
    background: #535353;
    box-shadow: 0.05em 0.1em 0.5em rgb(0, 0, 0, 0.4);
    font-weight: 600;
    align-items: center;
    padding-left: 1.2em;
    position: relative;
    margin: .5em;
    margin-bottom: 0;
    border-top: none;
    display: flex;
    border-left: none;
    overflow: hidden;
    ${props => props.selected ? `
        outline: .3rem solid #F38011;
    ` : ``}
    &:active {
        box-shadow: 0 0 1em rgba(0, 0, 0, 0.2) inset;
    }
    padding-right: ${levelMeterWidth};
    &:after {
        content: "";
        background: ${props => props.color};
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        width: .6em;
    }
    ${props => props.mute ? `
        opacity: 0.3;
    ` : ``}
    &:before {
        content: "";
        background: #2E2E2E;
        position: absolute;
        right: 0;
        bottom: 0;
        top: 0;
        width: ${levelMeterWidth};
    }
`
const TrackStatus = styled.div`
    display: flex;
    align-items: center;
    > * {
        margin: 0 .5em;
        color: ${props => props.mainColor};
    }
`
const TrackName = styled.div`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding: .3em .3em .3em 0;
    flex-shrink: 0;
    max-width: 10em;
`
const TrackActionWrap = styled.div`
    flex-shrink: 0;
    display: flex;
`
const LevelMeter = styled.div`
position: absolute;
right: 0;
bottom: 0;
width: ${levelMeterWidth};
height: 100%;
background: linear-gradient(#FF6900 0%, #FF6900 36%, #FFDE42 36%, #FFDE42 48%, #8CCB2A 48%, #8CCB2A 83%, #1E5F21 100%);
`
const Children = styled.div`
padding: 0 0 0 2em;
display: flex;
flex-direction: column;
flex-wrap: wrap;
`
const TracksOfInterest = styled.div`
    padding: .5em;
    color: white;
    width: 12rem;
    flex-shrink: 0;
    flex-grow: 0;
`
const TrackAndChildren = styled.div`

`
let touchDown = new Date()
const TouchedTrackComposer = compose(watch(touchedTrack), provide(props => {
    return {
        touchedTrack: touchedTrack.get()
    }
}))

const withTouchedTrack = component => TouchedTrackComposer(component)

class Track extends React.Component<any, any> {
    render() {
        const { track } = this.props

        const onTouchDown = () => {
            touchDown = new Date()
            touchedTrack.set(track)
        }
        
        const onTouchEnd = () => {
            touchedTrack.set(null)
            if (new Date().getTime() - touchDown.getTime() > 300) {
                return
            } else {
                post(`tracks/${track.name}`, { selected: true })
            }
        }

        return <TrackAndChildren>
            <TrackWrap className={`track track/${this.props.track.index}`} {...track} onTouchStart={onTouchDown} onTouchEnd={onTouchEnd}>
                <TrackName>{track.name}</TrackName>
                <Spacer />
                <TrackStatus>
                    {TrackActions.filter(action => resolve(track, action.path) && action.path !== 'arm').map(action => {
                      return action.icon ? <FontAwesomeIcon icon={action.icon} /> : action.character
                    })}
                </TrackStatus>
                {track.type !== 'Group' ? <TrackActionWrap>
                    {TrackActions.filter(action => action.path === 'arm').map(action => {
                        const props = {
                            active: resolve(track, action.path),
                            onTouchStart: (event) => {
                                event.stopPropagation()
                                post(`tracks/${track.name}`, { [action.path]: !resolve(track, action.path) })
                            },
                            ...action
                        }

                        return <TrackButton key={action.path} {...props}>{action.icon ? <FontAwesomeIcon icon={action.icon} /> : action.character}</TrackButton>
                    })} 
                </TrackActionWrap> : null}
                <LevelMeter className={`levelMeter track/${this.props.track.index}`} />
            </TrackWrap>
            {track.children.length && !this.props.hideChildren ? <Children>
                {track.children.map(child => {
                    let childCopy = {...child}
                    childCopy.mute = track.mute
                    return <Track key={child.name} track={childCopy} />
                })}

            </Children> : null}
        </TrackAndChildren>
    }
}

const TracksWrap = styled.div`
    flex-grow: 1;
    position: relative;
    height: 100%;
`
const Inner = styled.div`
    position: absolute;
    flex-wrap: wrap;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
    display: flex;
    flex-direction: column;
`

const BigTrackWrap = styled.div`
z-index: 2;
align-items: center;
justify-content: center;
font-size: 1.2em;
padding: 2em 30vw;
background: #222;
`
const RecordingMessage = styled.div`

text-align: center;
margin-bottom: 2em;
font-weight: 600;
`

if (navigator.userAgent.toLowerCase().indexOf('mac') === -1) {
    window.addEventListener('contextmenu', function (evt) {
        evt.preventDefault();
    });
}

const BigTrack = ({ track }) => {
    return <BigTrackWrap>
        <RecordingMessage>Recording...</RecordingMessage>
        <Track key={track.name} track={track} />
    </BigTrackWrap>
}

const Tracks = withBitwigState(({ bitwig }) => {
    const { tracks } = bitwig

    return <TracksWrap>
        <Inner>
            {tracks.filter(t => t.type !== 'Effect' && t.type !== 'Master').map(track => <Track key={track.name} track={track} />)}
        </Inner>
    </TracksWrap>
})

const TrackActions = [
    {
        path: 'mute',
        character: 'M'
    },
    {
        path: 'solo',
        character: 'S',
        mainColor: '#D8CE00'
    },
    {
        path: 'arm',
        mainColor: '#F00808',
        icon: icons.faCircle
    }
]
const ButtonSize = '4em'
const TrackButton = styled.div`
    width: ${ButtonSize};
    height: ${ButtonSize};
    font-size: .5em;
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    ${(props: any) => props.active ? `
        background: ${props.mainColor || '#F38011'};
        color: black;
    `: ``}
    &:active {
        box-shadow: 0 0 4rem rgba(0, 0, 0, 0.2) inset;
    }


        border-left: 2px solid rgba(0, 0, 0, 0.2);

`
const TinyButtonSize = '4em'
const TinyButton = styled.div`
    width: ${TinyButtonSize};
    height: ${TinyButtonSize};
    font-size: 1em;
    display: flex;
    margin: .5em 1em;
    align-items: center;
    justify-content: center;
    color: white;
    ${props => props.active ? `
    
    
    background: ${props.mainColor || '#F38011'};
    color: black;
    
    ` : ``}
    &:active {
        box-shadow: 0 0 4rem rgba(0, 0, 0, 0.2) inset;
    }
    box-shadow: 0.05em 0.1em 0.5em rgb(0, 0, 0, 0.4);
`

const VerticalButtonsz = withTouchedTrack(class Buttonz extends React.Component {
    state = {
        volume: 0
    }
    lastTrackName
    componentWillReceiveProps(props) {
        if (props.track && this.lastTrackName !== props.track.name) {
            this.setState({
                volume: props.track.volume
            })
            this.lastTrackName = props.track.name
        }
    }
    handleVolumeChange = (event) => {
        const volume = event / 100
        post(`tracks/${this.lastTrackName}`, {volume})
        this.setState({
            volume
        })
    }
    render() {
        const props = this.props
        const track = (props.touchedTrack || props.track) as any
        return <VerticalButtons>
            {TrackActions.map(action => {
                    const props = {
                        onTouchStart: (event) => {
                            event.stopPropagation()
                            post(`tracks/${track.name}`, { [action.path]: !resolve(track, action.path) })
                        },
                        active: resolve(track, action.path),
                        ...action
                    }
    
                    return <TinyButton key={action.path} {...props}>{action.icon ? <FontAwesomeIcon icon={action.icon} /> : action.character}</TinyButton>
            })}
            <Slider handleStyle={{height: '30px', width: '30px', marginLeft: '-13px'}} style={{height: '18rem', margin: '1em 0'}} vertical min={0} max={100} onChange={this.handleVolumeChange} value={this.state.volume * 100} />
        </VerticalButtons>
    }
})

const Class = class Tablet extends React.Component<any, Tablet.State> {

    componentWillMount() {
        if (document.body.requestFullscreen) {
            document.body.requestFullscreen();
        }
    }
    render() {
        const { bitwig } = this.props
        const armedTracks = bitwig.tracks.filter(track => track.arm)
        const recording = bitwig.recording && armedTracks.length
        const tracksOfInterest = bitwig.allTracks.filter(track => (track.solo || track.arm || track.mute) && track.name.indexOf('Master') === -1)
        const track = bitwig.allTracks.filter(track => track.selected)[0]

        return <HorizontalWrap>
            <TracksOfInterest>
                {tracksOfInterest.map(track => <Track hideChildren={true} track={track} key={track.name} />)}
            </TracksOfInterest>
            <Wrap>
                <Tracks />
                {recording ? <BigTrack track={armedTracks[0]} /> : null}

                <Controls />
            </Wrap>
            <VerticalButtonsz track={track} />
            <ReloadButton onClick={() => window.location.reload()}>
                <FontAwesomeIcon icon={icons.faUndo} />
            </ReloadButton>
        </HorizontalWrap>
    }
}

export const Tablet = withBitwigState(Class)
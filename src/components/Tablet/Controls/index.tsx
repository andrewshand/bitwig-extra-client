import * as icons from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { resolve } from '@webantic/util/object';
import * as React from 'react';
import styled, {css} from 'styled-components';
import * as _ from 'underscore';
import { BitwigState, post, withBitwigState } from '../../Bitwig';
export namespace Controls {
    export interface Props {
        bitwig: BitwigState
    }

    export interface State {

    }
}

const AppActions = [
    {
        path: 'undo',
        icon: icons.faUndo
    },
    {
        path: 'redo',
        icon: icons.faRedo
    },
    {
        path: 'transport.looping',
        icon: icons.faRecycle
    },

    // {
    //     path: 'transport.rewind',
    //     icon: icons.faBackward
    // },
    // {
    //     path: 'transport.fastForward',
    //     icon: icons.faForward
    // },
    {
        path: 'transport.playing',
        icon: icons.faPlay
    },
    {
        path: 'metronome',
        icon: icons.faStopwatch
    },
    {
        path: 'recording',
        mainColor: '#F00808',
        icon: icons.faCircle
    },
]
const ButtonSize = '4em'
const Button = styled.div`
    width: ${ButtonSize};
    height: ${ButtonSize};
    font-size: 1.8em;
    margin: .2em;
    display: flex;
    background: linear-gradient(#808080, #646464);
    align-items: center;
    justify-content: center;
    color: white;
    ${(props: any) => props.active ? `
        background: ${props.mainColor || '#F38011'};
        color: black;
    `: ``}
    &:active {
        box-shadow: 0 0 4rem rgba(0, 0, 0, 0.2) inset;
    }
    box-shadow: 0.05em 0.1em 0.5em rgb(0, 0, 0, 0.4);

`
const LoopGeneric = styled.div`
position: absolute;
border: .2em solid white;
top: 0;
bottom: 0;
width: 10%;
`
const StartLoop = styled(LoopGeneric)`
left: 0;
border-right: none;
`
const EndLoop = styled(LoopGeneric)`
border-left: none;
right: 0;
`
const ButtonsWrao = styled.div`

display: flex;
flex-shrink: 0;
width: 100%;
justify-content: center;

`
const Wrap = styled.div`



`
const Beat = styled.div`
    display: inline-flex;
    width: 100%;
    height: 3em;
    width: 3em;
    border: .1em solid #666;
    border-left: none;
    position: relative;
    font-size: 1.5em;
    align-items: center;
    justify-content: center;
    color: #888;
    ${props => props.inLoop ? css`background: rgba(255, 255, 255, .1);` : ``}
`
const Timeline = styled.div`
position: relative;
white-space: nowrap;
`
const ScrollWrap = styled.div`
padding-top: 2em;
position: relative;
overflow-x: scroll;
white-space: nowrap;

`
const Marker = styled.div`

position: absolute;
height: 100%;
width: 4px;
background: white;
top: 0;
bottom: 0;

`
const Go = styled(FontAwesomeIcon)`
    ${props => props.back ? `
        left: 1em;
    ` : `
        right: 1em;
    `}
    position: absolute;
    font-size: 2em;
    top: 50%;
    transform: translateY(-50%);
    opacity: 0;
    transition: opacity .5s;

    @keyframes glow {
        0% {
            transform: translateY(-50%) scale(1);
        }
        50% {
            transform: translateY(-50%) scale(1.2);
        }
        100% {
            transform: translateY(-50%) scale(1);
        }
    }
    animation-name: glow;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    /* animation-direction: alternate; */
` as any
const beats = _.times(200, i => i)
const Class = class Controls extends React.Component<any, Controls.State> {

    render() {
        const scrollMarkerIntoView = () => {
            document.getElementById('marker').scrollIntoView({
                behavior: 'smooth'
            })
        }
        const { transport } = this.props.bitwig
        console.log(transport)
        return <Wrap>
            <ButtonsWrao>
                {AppActions.map(action => {
                    const props = {
                        active: resolve(this.props.bitwig, action.path),
                        onTouchStart: () => post(action.path, !resolve(this.props.bitwig, action.path)),
                        ...action
                    }

                    return <Button {...props}><FontAwesomeIcon icon={action.icon} /></Button>
                })}
            </ButtonsWrao>
            <Timeline id="timeline">
                <ScrollWrap>
                    <Marker id="marker" />
                    {beats.map(i => {
                        const loopStart = transport.in / 4
                        const loopEnd = transport.out / 4
                        const inLoop = i >= loopStart && i <= loopEnd
                        return <Beat inLoop={inLoop} onClick={() => post(`transport.position`, i * 4)} className="beat">
                            {i + 1}
                            {i === loopStart ? <StartLoop /> : null}
                            {i === loopEnd ? <EndLoop /> : null}
                        </Beat>
                    })}
                </ScrollWrap>
                <Go onClick={scrollMarkerIntoView} id="goLeft" back icon={icons.faBackward} />
                <Go onClick={scrollMarkerIntoView} id="goRight" forward icon={icons.faForward} />
            </Timeline>
        </Wrap>
    }
}

export const Controls = withBitwigState(Class)
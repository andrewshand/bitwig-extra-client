import { randomBytes } from 'crypto';
import { isUndefined } from 'util';
import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './style.css';
import { applyMiddleware } from 'redux';
import { MainSection } from '../MainSection'
import Knob from 'react-canvas-knob';
import { Slider } from 'react-toolbox/lib/slider';
import { Bitwig, post, onLevels } from '../Bitwig'
import * as _ from 'underscore';
import * as classnames from 'classnames';
import { getTrackTags, getTagglessName, TAG_CHAR, isUnusableTag } from '../tags'

export namespace EQItem {
    export interface Props {
        track: Bitwig.TrackPacket,
        eqing: boolean
    }

    export interface State {
        bands: Bitwig.EQBand[]
    }
}

export class EQComponent extends React.Component<EQItem.Props, EQItem.State> {

    canvas: HTMLCanvasElement
    canvasSize = 500
    mouseUpListener: any
    mouseMoveListener: any
    keyDownListener: any
    keyUpListener: any
    lastX: number
    lastY: number
    lastMouseDown: [Date, number]
    qDown: boolean = false
    qAtDown: number
    prevBandsId: string = ''
    remoteUpdater: Function
    controlPointColors = [
        '#E7324F',
        '#FBC323',
        '#39B9E2',
        '#9BDE57',
        '#AA5AD7'
    ]

    constructor(props?: EQItem.Props, context?: any) {
        super(props, context);
        this.state = {
            bands: []
        };
        this.remoteUpdater = _.throttle(() => {
            post(this.createTrackSelector(), { eq: this.props.track.eq })
        }, 10)
    }
    draggingIndex: number = -1
    componentDidMount() {
        this.updateCanvas()
        this.mouseUpListener = this.onMouseUp.bind(this)
        this.mouseMoveListener = this.onMouseMove.bind(this)
        this.keyUpListener = this.onKeyUp.bind(this)
        this.keyDownListener = this.onKeyDown.bind(this)
        window.addEventListener('mouseup', this.mouseUpListener)
        window.addEventListener('mousemove', this.mouseMoveListener)
        window.addEventListener('keyup', this.keyUpListener)
        window.addEventListener('keydown', this.keyDownListener)
    }
    componentWillUnmount() {
        window.removeEventListener('mouseup', this.mouseUpListener)
        window.removeEventListener('mousemove', this.mouseMoveListener)
        window.removeEventListener('keyup', this.keyUpListener)
        window.removeEventListener('keydown', this.keyDownListener)
    }
    onKeyUp(event: any) {
        if (event.keyCode === 81) {
            this.qDown = false
            this.qAtDown = -1
        }
    }
    onKeyDown(event: any) {
        if (event.keyCode === 81 && !this.qDown) {
            this.qDown = true
            if (this.draggingIndex !== -1) {
                this.qAtDown = this.state.bands[this.draggingIndex].q
            }
        }
    }
    componentDidUpdate() {
        this.updateCanvas()
    }
    componentWillReceiveProps(props) {
        this.setState(state => {
            return {
                ...state,
                bands: props.track.eq ? props.track.eq.bands : []
            }
        })
    }
    applyLowCut(mega: boolean, band: any, buckets: number[]) {
        const distance = mega ? 0.02 : 0.04
        const center = band.freq
        const max = center + distance
        const min = center - distance
        const centeredLevelOnZero = -1

        buckets.forEach(function (bucket, index) {
            const whereAreWe = index / buckets.length

            if (whereAreWe > max) {
                // no effect
            }
            else if (whereAreWe > min && whereAreWe < max) {
                const fractionThroughStepUp = (whereAreWe - min) / (max - min)
                buckets[index] += centeredLevelOnZero * (1 - fractionThroughStepUp)
            }
            else {
                buckets[index] += centeredLevelOnZero
            }
        })
    }
    applyHighCut(mega: boolean, band: any, buckets: number[]) {
        const distance = mega ? 0.02 : 0.04
        const center = band.freq
        const max = center + distance
        const min = center - distance
        const centeredLevelOnZero = -1

        buckets.forEach(function (bucket, index) {
            const whereAreWe = index / buckets.length

            if (whereAreWe < min) {
                // no effect
            }
            else if (whereAreWe > min && whereAreWe < max) {
                const fractionThroughStepUp = (whereAreWe - min) / (max - min)
                buckets[index] += centeredLevelOnZero * fractionThroughStepUp
            }
            else {
                buckets[index] += centeredLevelOnZero
            }
        })
    }
    applyLowShelf(band: any, buckets: number[]) {
        const distance = 0.08
        const center = band.freq
        const max = center + distance
        const min = center - distance
        const centeredLevelOnZero = (band.amp - 0.5) * 2

        buckets.forEach(function (bucket, index) {
            const whereAreWe = index / buckets.length

            if (whereAreWe > max) {
                // no effect
            }
            else if (whereAreWe > min && whereAreWe < max) {
                const fractionThroughStepUp = (whereAreWe - min) / (max - min)
                buckets[index] += centeredLevelOnZero * (1 - fractionThroughStepUp)
            }
            else {
                buckets[index] += centeredLevelOnZero
            }
        })
    }
    getEQType(bandIndex: number, type: number) {
        const moduloed = bandIndex % 5
        if (moduloed === 0 || moduloed === 4) {
            const lowHigh = moduloed === 0 ? 'low' : 'high'

            if (type < .25) {
                return `${lowHigh}-cut-12`
            }
            if (type < .5) {
                return `${lowHigh}-cut-24`
            }
            if (type < .75) {
                return 'bell'
            }
            return `${lowHigh}-shelf`
        }
        return 'bell'
    }
    applyHighShelf(band: any, buckets: number[]) {
        const distance = 0.08
        const center = band.freq
        const max = center + distance
        const min = center - distance
        const centeredLevelOnZero = (band.amp - 0.5) * 2

        buckets.forEach((bucket, index) => {
            const whereAreWe = index / buckets.length

            if (whereAreWe < min) {
                // no effect
            }
            else if (whereAreWe > min && whereAreWe < max) {
                const fractionThroughStepUp = (whereAreWe - min) / (max - min)
                buckets[index] += centeredLevelOnZero * fractionThroughStepUp
            }
            else {
                buckets[index] += centeredLevelOnZero
            }
        })
    }
    getBell(fraction: number) {
        // for (let i = 0; i < 100; i++) {
        //     var fraction = i / 100
        //     var howMuchPower = .5
        //     var evener = 1 / Math.pow(.5, howMuchPower)
        //     var halfThrough = fraction < .5 ? fraction : 1 - fraction
        //     numbers.push(Math.pow(halfThrough, howMuchPower) * evener)
        // }
        const howMuchpower = 2
        const evener = 1 / Math.pow(1, howMuchpower)
        return Math.pow(fraction, howMuchpower) * evener
    }
    applyBell(band: any, buckets: number[]) {
        const centeredLevelOnZero = (band.amp - 0.5) * 2
        const bellSpread = (1 - band.q) // can only ever reach .8 or so of the graph
        buckets.forEach((bucket, index) => {
            const whereAreWe = index / buckets.length
            const distanceFromCenter = Math.abs(whereAreWe - band.freq) / bellSpread
            const bellAmpAtThisPoint = this.getBell(Math.max(1 - distanceFromCenter, 0)) * centeredLevelOnZero
            buckets[index] += bellAmpAtThisPoint
        })
    }
    getBuckets(): number[] {
        const bucketAmount = 100
        const buckets = new Array(bucketAmount)
        buckets.fill(0)
        const bands = this.state.bands
        bands.forEach((band, index) => {
            const type = band.type
            if (!band.on) {
                return
            }

            const stringtype = this.getEQType(index, type)
            const handlers = {
                'high-shelf': this.applyHighShelf.bind(this),
                'low-shelf': this.applyLowShelf.bind(this),
                'high-cut-12': this.applyHighCut.bind(this, false),
                'low-cut-12': this.applyLowCut.bind(this, false),
                'high-cut-24': this.applyHighCut.bind(this, true),
                'low-cut-24': this.applyLowCut.bind(this, true),
                'bell': this.applyBell.bind(this)
            }
            handlers[stringtype](band, buckets)
        })

        return buckets
    }
    updateCanvas() {
        const ctx = this.canvas.getContext('2d');

        const bands = this.state.bands
        const id = JSON.stringify(bands)
        if (id === this.prevBandsId) {
            return
        }
        this.prevBandsId = id
        const buckets = this.getBuckets()
        ctx.clearRect(0, 0, this.canvasSize, this.canvasSize);
        ctx.beginPath();

        ctx.lineWidth = 8;
        function valueForBucket(bucket) {
            return Math.min(Math.max(0, (bucket + 1) / 2), 1)
        }
        buckets.forEach((bucket, index) => {
            const fractionalIndex = index / buckets.length
            const value = valueForBucket(bucket)
            const oppositeY = this.canvasSize - (value * this.canvasSize)
            if (index === 0) {
                ctx.moveTo(0, oppositeY)
            }
            else {
                ctx.lineTo(fractionalIndex * this.canvasSize, oppositeY)
            }
        })

        ctx.strokeStyle = "#BBB";
        ctx.stroke()
        ctx.lineTo(this.canvasSize, this.canvasSize)
        ctx.lineTo(0, this.canvasSize)
        const value = valueForBucket(buckets[0])
        const oppositeY = this.canvasSize - (value * this.canvasSize)
        ctx.lineTo(0, oppositeY)
        ctx.fill()
    }
    onMouseUp(event: any) {
        this.draggingIndex = -1
        this.lastX = -99999
    }
    onMouseMove(event: any) {
        if (this.draggingIndex === -1) {
            return
        }
        const canvasBounding = this.canvas.getBoundingClientRect()
        var X = event.pageX - canvasBounding.left
        var Y = event.pageY - canvasBounding.top
        const qY = Y - this.lastY
        if (this.lastX === -99999 || !this.qDown) {
            this.lastX = X
            this.lastY = Y
        }

        if (this.qDown) {
            this.setState(state => {
                state.bands[this.draggingIndex].q = parseFloat(Math.min(1, Math.max(this.qAtDown - (qY / this.canvas.offsetHeight), 0)).toFixed(3))
                return state
            })
        }
        else {
            this.setState(state => {
                state.bands[this.draggingIndex].freq = parseFloat(Math.min(1, Math.max(X / this.canvas.offsetWidth, 0)).toFixed(3))
                state.bands[this.draggingIndex].amp = parseFloat((1 - Math.min(1, Math.max(Y / this.canvas.offsetHeight, 0))).toFixed(3))
                return state
            })
        }
        this.remoteUpdater()
    }
    createTrackSelector(): any {
        return { track: { index: this.props.track.index } }
    }
    onContextMenu(index: number, event: any) {
        event.preventDefault()
        event.stopPropagation()

        this.setState(state => {
            let amount: number = .25
            if (index % 5 === 0 || index % 5 === 4) {
                const rounded = Math.floor(state.bands[index].type * 4) / 4
                amount = (rounded + .35) % 1
                // amount = 0 // low pass
                // amount = .2 // low pass hard
                // amount = .4 // still low pass hard
                // amount = .5 // bell
                // amount = .7 // still bell
                // amount = .85 // shelf!
            }
            else {
                const rounded = Math.round(state.bands[index].type * 2) / 2
                amount = (rounded + .5) % 1
            }
            state.bands[index].type = amount
            this.remoteUpdater()
            return state
        })
    }
    onMouseDownControlPoint(index: number, event: any) {
        event.stopPropagation()
        post({ track: { index: this.props.track.index } }, { selected: true })

        if (this.lastMouseDown != null && new Date().getTime() - this.lastMouseDown[0].getTime() < 200 && this.lastMouseDown[1] === index) {
            this.setState(state => {
                state.bands[index].on = !state.bands[index].on
                return state
            })
            this.remoteUpdater()
        }
        else {
            this.draggingIndex = index
            this.qAtDown = this.state.bands[this.draggingIndex].q
        }

        this.lastMouseDown = [new Date(), index]
    }
    renderControlPoints() {
        if (this.props.eqing) {
            return this.state.bands.map((band, index) => {
                const classes = classnames(style.controlPoint, { [style['-off']]: !band.on })
                const theStyle = {
                    left: (band.freq * 100) + "%",
                    top: ((1 - band.amp) * 100) + "%",
                    background: this.controlPointColors[index % 5]
                }
                return <div key={index} className={classes} onContextMenu={this.onContextMenu.bind(this, index)} onMouseDown={this.onMouseDownControlPoint.bind(this, index)} style={theStyle}>
                    {index + 1}
                </div>
            })
        }
    }
    reset() {
        this.setState(state => {

            this.remoteUpdater()
            return {
                ...state,
                bands: _.times(state.bands.length, i => {
                    return {
                        freq: (i % 5) / 5,
                        amp: .5,
                        q: .32,
                        on: true,
                        type: (i % 5 === 0 || i % 5 === 4) ? 1 : 0
                    }
                })
            }
        })
    }
    onDoubleClickWholeThing(event) {
        if (this.state.bands.length === 0) {
            post({ track: { index: this.props.track.index } }, { selected: true })
            post({ 'app': 'insertAtEnd' })
        }
    }
    render(): JSX.Element {
        const classes = classnames(style.eq, {
            [style['-eqing']]: this.props.eqing
        })
        return <div className={classes} onDoubleClick={this.onDoubleClickWholeThing.bind(this)}>
            <canvas width={this.canvasSize} height={this.canvasSize} ref={ref => this.canvas = ref}></canvas>
            {this.renderControlPoints()}
        </div>
    }
}

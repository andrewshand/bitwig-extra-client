import { isUndefined } from 'util';
import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './style.css';
import { applyMiddleware } from 'redux';
import { MainSection } from '../MainSection'
import Knob from 'react-canvas-knob';
import { EQComponent } from '../EQComponent'
import { Slider } from 'react-toolbox/lib/slider';
import { Bitwig, post, onLevels } from '../Bitwig'
import * as _ from 'underscore';
import * as classnames from 'classnames';
import { getTrackTags, getTagglessName, TAG_CHAR, isUnusableTag } from '../tags'

export namespace TodoItem {
  export interface Props {
    track: Bitwig.TrackPacket,
    availableTags: string[]
    selectTrack: Function,
    deselectTrack: Function,
    selected: boolean
    inTag: string,
    multipleSelected: boolean,
    eqing: boolean
  }

  export interface State {
    volume: number;
    mute: boolean;
    solo: boolean;
    optionsOpen: boolean;
    editingName: boolean;
  }
}

export class TrackComponent extends React.Component<TodoItem.Props, TodoItem.State> {


  levelsHandle: any
  windowKeyListener: any
  levelMeter: HTMLElement
  nameElement: HTMLElement

  constructor(props?: TodoItem.Props, context?: any) {
    super(props, context);
    this.state = {
      volume: props.track.volume,
      solo: props.track.solo,
      mute: props.track.mute,
      optionsOpen: false,
      editingName: false
    };
  }
  offsetForEvent(event) {
    if (event.shiftKey) {
      return 0.005
    }
    if (event.metaKey) {
      return 0.03
    }
    return 0.025
  }
  onVolumeDown(event) {
    event.stopPropagation()
    this.handleVolumeChange(Math.max(0, this.props.track.volume - this.offsetForEvent(event)));
  }
  onVolumeUp(event) {
    event.stopPropagation()
    this.handleVolumeChange(Math.min(1, this.props.track.volume + this.offsetForEvent(event)));
  }
  componentWillReceiveProps(nextProps) {
    this.setState(s => {
      const state = { ...s }
      state.volume = nextProps.track.volume
      state.solo = nextProps.track.solo
      state.mute = nextProps.track.mute
      if (!nextProps.selected) {
        state.optionsOpen = false
      }
      return state
    })
    if (this.nameElement) {
      this.nameElement.innerText = getTagglessName(nextProps.track)
    }
  }
  componentWillMount() {
    this.levelsHandle = onLevels((levels) => {
      if (this.levelMeter) {
        const between0And1 = levels[this.props.track.index] / 128
        this.levelMeter.style.transform = `scaleY(${between0And1})`
      }
    })
    this.windowKeyListener = this.onKeyDown.bind(this)
    window.addEventListener('keydown', this.windowKeyListener)
  }
  placeCaretAtEnd(el) {
    el.focus();
    let docBody = document.body as any
    if (typeof window.getSelection !== "undefined"
      && typeof document.createRange !== "undefined") {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    } else if (typeof docBody !== "undefined") {
      var textRange = docBody.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
    }
  }
  componentWillUnmount() {
    this.levelsHandle.off()
    window.removeEventListener('keydown', this.windowKeyListener)
  }
  getThisTrackSelector() {
    return {
      track: { index: this.props.track.index }
    }
  }
  toggleOptions() {
    this.setState({ optionsOpen: !this.state.optionsOpen })
  }
  onMuteOrSolo(event: any, muteOrSolo: 'mute' | 'solo') {
    event.stopPropagation()
    const { solo, mute, index } = this.props.track
    const keyToCheck = muteOrSolo === 'mute' ? mute : solo
    if (event.shiftKey) {
      if (keyToCheck) {
        // unsolo everything
        post({ track: {} }, { [muteOrSolo]: false })
        const newState = { [muteOrSolo]: false } as any
        this.setState(newState)
      }
      else {
        // solo just this thing
        post({ track: { index: { ne: index } } }, { [muteOrSolo]: false })
        post({ track: { index } }, { [muteOrSolo]: true })
        const newState = { [muteOrSolo]: true } as any
        this.setState(newState)
      }
    }
    else {
      // reverse current solo
      post({ track: { index } }, { [muteOrSolo]: !this.state[muteOrSolo] })
      this.setState((s) => {
        const state = { ...s }
        state[muteOrSolo] = !state[muteOrSolo]
        return state
      })
    }
  }
  onSolo(event) {
    this.onMuteOrSolo(event, 'solo');
  }
  onMute(event) {
    this.onMuteOrSolo(event, 'mute');
  }
  handleVolumeChange(value) {
    this.setState(s => {
      const state = { ...s }
      state.volume = value
      post({
        track: {
          index: this.props.track.index
        }
      }, {
          volume: value
        })
      return state
    })
  }
  onKeyDownName(event: any) {
    event.stopPropagation()
  }
  onKeyUpName(event: any) {
    const cancel = () => {
      this.cancelEditingName()
    }
    if (event.keyCode === 13) {
      // enter === commit
      event.preventDefault() // don't add an actual new line
      event.stopPropagation() // stop it from triggering keydown to initiate editing again
      if (event.target.innerText.trim().length === 0) {
        return cancel()
      }
      let newName = event.target.innerText.trim() + ` ${getTrackTags(this.props.track).map(function (tag) {
        return `${TAG_CHAR}${tag}`
      }).join(' ')}`
      newName = newName.replace(/(?:\r\n|\r|\n)/g, '')
      post(this.getThisTrackSelector(), { name: newName })
      this.setState({ editingName: false })
      this.nameElement.blur()
      this.nameElement.innerText = newName
    }
    else if (event.keyCode === 27) {
      // escape === cancel
      cancel()
    }
    else if (event.keyCode === 32) {
      event.stopPropagation()
      // stop spaces from bubbling up
    }
  }
  renderOptions() {
    if (!this.state.optionsOpen) {
      return null
    }
    return <div className={style.options}>
      {this.renderTags()}
    </div>
  }
  getImage() {
    const name = getTagglessName(this.props.track)
    const map = {
      snare: 'https://static.wixstatic.com/media/4083ca_61ff13c249aa4446acbbf56411d339a2.png/v1/fill/w_784,h_503,al_c,usm_0.66_1.00_0.01/4083ca_61ff13c249aa4446acbbf56411d339a2.png',
      kick: `http://lossenderosstudio.com/img/kickdrum.gif`,
      hat: `https://qph.ec.quoracdn.net/main-qimg-0e3d263d03086f3342db43cc20677244`,
      piano: `http://pngimg.com/uploads/piano/piano_PNG10897.png`,
      sax: `http://www.pngall.com/wp-content/uploads/2016/06/Saxophone-PNG-Clipart.png`,
      crash: `https://www.drumcenternh.com/media/catalog/product/cache/1/thumbnail/460x/040ec09b1e35df139433887a97daa66f/2/0/20-A-Zildjian-Medium-Thin-Crash_1.png`,
      vocal: `https://i1.sndcdn.com/avatars-000329995618-v8g3ek-t500x500.jpg`,
      acoustic: `https://upload.wikimedia.org/wikipedia/commons/4/45/GuitareClassique5.png`,
      'bass guitar': `http://www.deanguitars.com/images/productimages/e5fmtbks/e5fmtbks.png`,
      electric: `http://assets.fender.com/frl/1f7e5e4bcebfc039e6b85cfd207b00eb/generated/05b9e02894f0151930fb36c6483967d5.png`,
      guitar: `http://assets.fender.com/frl/1f7e5e4bcebfc039e6b85cfd207b00eb/generated/05b9e02894f0151930fb36c6483967d5.png`,
      clap: `http://www.pngall.com/wp-content/uploads/2016/03/Hands-PNG-8.png`,
      click: `http://pngimg.com/uploads/hands/hands_PNG954.png`,
      organ: `http://allenorganstudioaugusta.com/Organs/Images/TH300.png`,
      flute: `http://www.pngall.com/wp-content/uploads/2016/06/Flute-PNG-Image.png`,
      tom: `http://www.codadrums.com/images/drum.png`,
      tycho: `https://f4.bcbits.com/img/a0978734060_16.jpg`,
      marimba: `http://www.bestdrumstore.com/media/com_eshop/products/resized/Marimba_Lyoma%2061-500x500.png`,
      burial: `https://f4.bcbits.com/img/0007035661_10.jpg`,
      trumpet: `http://floridatrumpets.com/wp-content/uploads/2015/11/Bach-19037.png`,
      cowbell: `https://vignette4.wikia.nocookie.net/csydes-test/images/0/0e/Cowbell.PNG/revision/latest?cb=20170207033327`,
      kalimba: `http://kalimbaa.com/wp-content/uploads/2014/12/Acoustic-Kalimba-200x200.png`,
      synth: `http://www.synth-project.de/crbst_dark_schraeg0.png?v=1r9imk1ah6ei2wb9`,
      keys: `http://www.synth-project.de/crbst_dark_schraeg0.png?v=1r9imk1ah6ei2wb9`,
      keyboard: `http://www.synth-project.de/crbst_dark_schraeg0.png?v=1r9imk1ah6ei2wb9`
    }
    for (const key in map) {
      if (new RegExp(`(^|\\s+)${key}s?(\\s+|$)`).test(name.toLowerCase())) {
        return map[key]
      }
    }
    return ''
  }
  getFilteredName() {
    return getTagglessName(this.props.track)
  }
  hasTag(tag) {
    return _.contains(getTrackTags(this.props.track), tag)
  }
  addTag(tag) {
    if (isUnusableTag(tag)) {
      return
    }
    let newName = this.props.track.name + ` ${TAG_CHAR}${tag}`
    post(this.getThisTrackSelector(), { name: newName })
  }
  removeTag(tag) {
    if (isUnusableTag(tag)) {
      return
    }
    let newName = this.props.track.name.replace(`${TAG_CHAR}${tag})`, '')
    newName = newName.replace(/[ ]{2,}/g, ' ') // remove more than two spaces in a row
    post(this.getThisTrackSelector(), { name: newName })
  }
  toggleTag(tag) {
    if (isUnusableTag(tag)) {
      return
    }
    if (!this.hasTag(tag)) {
      this.addTag(tag)
    }
    else {
      this.removeTag(tag)
    }
  }
  onNewTagInputKeyUp(event: any) {
    if (event.keyCode === 13) {
      // enter === commit
      if (event.target.value.trim().length === 0) {
        return
      }
      this.addTag(event.target.value)
      event.target.value = ''
    }
    else if (event.keyCode === 27) { // escape
      this.cancelEditingName();
    }
  }
  cancelEditingName() {
    this.setState({ editingName: false })
    if (this.nameElement) {
      this.nameElement.innerText = getTagglessName(this.props.track)
      this.nameElement.blur()
    }
  }
  selectTrack(event) {
    if (this.props.selected && event.metaKey) {
      this.props.deselectTrack()
    }
    else {
      this.props.selectTrack(event.metaKey)
    }
  }
  startEditingName() {
    this.setState({ editingName: true })
    if (this.nameElement) {
      this.nameElement.innerText = getTagglessName(this.props.track)
      this.nameElement.focus()
      this.placeCaretAtEnd(this.nameElement)
      document.execCommand('selectAll', false, null)
    }
  }
  renderTags() {
    return <div className={style.tags}>
      {this.props.availableTags.map(tag => {
        return <div onClick={this.toggleTag.bind(this, tag)} className={classNames(style.tag, this.hasTag(tag) ? style['-active'] : null)}>
          <div className={style.checkbox}></div>
          <div className={style.tagName}>{tag}</div>
        </div>
      })}
      <input type="text" placeholder="New tag" onKeyUp={this.onNewTagInputKeyUp.bind(this)} />
    </div>
  }
  onKeyDown(event) {
    if (event.metaKey && !event.shiftKey && event.keyCode === 82 && this.props.selected) { // cmd + r
      this.startEditingName()
      event.preventDefault()
      event.stopPropagation()
    }
    if (this.props.selected) {
      // Left: 37
      // Up: 38
      // Right: 39
      // Down: 40
      if (event.keyCode === 38 || event.keyCode === 39) {
        this.onVolumeUp(event)
        event.preventDefault()
      }
      if (event.keyCode === 40 || event.keyCode === 37) {
        this.onVolumeDown(event)
        event.preventDefault()
      }
      if (event.keyCode === 78) {
        this.onSolo(event)
        event.preventDefault()
      }
      if (event.keyCode === 77) {
        this.onMute(event)
        event.preventDefault()
      }
    }
  }
  render() {
    const { track, selected, inTag, multipleSelected, eqing } = this.props
    const { volumeString, color, type } = track
    const { volume, mute, solo } = this.state
    const taglessName = getTagglessName(track)
    const notSelected = multipleSelected && !selected
    const images = {
      group: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAbCAYAAACEP1QvAAAKqWlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUU2kWx7/30hstIdIJvSO9Sq+h9yYqIaGEEkIgqNiVwREcUUREQB1RqQqOSpFRRCzYBgGlWAdkEFDXwYKoqOwDlrCze3b37D/n5v7Ozffuu+/L+875A0AeZPH5KbAEAKm8TEGQhzMjIjKKgRsGEICBFNAFcix2Bt8pIMAHIFrMf9XHfmQ1ovsGc73+/ff/KklOXAYbACgA4VhOBjsV4fNItLH5gkwAUEgAtbWZ/DkuQZgmQAZE+NQcJyxw+xzHLvCD+TUhQS4IjwOAJ7NYggQASB+QOiOLnYD0IdMQNuJxuDyEXRG2ZyeyOAjnIKyfmpo2x2cQ1o79pz4Jf+kZK+rJYiWIeOFZ5oV35WbwU1jr/8/t+N9KTREu3kMVCXKiwDMIyXRkz6qT07xFzIv1819kLmd+/TwnCj1DF5md4RK1yByWq/ciC5NDnRaZJVi6lpvJDFlkQVqQqD8vxc9H1D+OKeK4DLfgRY7nujMXOTsxJHyRs7hhfouckRzsvbTGRVQXCINEM8cL3EXPmJqxNBubtXSvzMQQz6UZIkTzcOJc3UR1XqhoPT/TWdSTnxKwNH+Kh6iekRUsujYTecEWOYnlFbDUJ0C0P8AVuAEf5MMAocAEmANjJJCpMuPWzb3TwCWNv17ATUjMZDghpyaOweSxDfUZJkbGlgDMncGFv/j94PzZguj4pVpaNwCWNQhULdVYMQC0ILshrbZU0zgJgPgfAFxis4WCrIUaeu4LA4hAHNCALFACakAbGCDzWQBb4IhM7AX8QQiIBKsBGySCVCAAa8FGsA3kgnywFxwApeAoOA6qwWlwFjSDi+AKuAHugG7QBx6DITAKXoFJ8BHMQBCEgygQFZKFlCENSA8ygawge8gN8oGCoEgoBkqAeJAQ2gjtgPKhQqgUOgbVQL9AF6Ar0C2oB3oIDUMT0DvoC4yCyTANVoQ14eWwFewEe8Mh8Co4AU6Hs+EceA9cAlfAp+Am+Ap8B+6Dh+BX8BQKoEgoOkoFZYCyQrmg/FFRqHiUALUZlYcqRlWg6lGtqE7UfdQQ6jXqMxqLpqIZaAO0LdoTHYpmo9PRm9G70aXoanQT+hr6PnoYPYn+jqFgFDB6GBsMExOBScCsxeRiijGVmEbMdUwfZhTzEYvF0rFaWEusJzYSm4TdgN2NPYxtwLZje7Aj2CkcDieL08PZ4fxxLFwmLhd3CHcKdxnXixvFfcKT8Mp4E7w7PgrPw2/HF+Nr8W34XvwYfoYgQdAg2BD8CRzCekIB4QShlXCPMEqYIUoStYh2xBBiEnEbsYRYT7xOfEJ8TyKRVEnWpEASl7SVVEI6Q7pJGiZ9JkuRdcku5GiykLyHXEVuJz8kv6dQKJoUR0oUJZOyh1JDuUp5RvkkRhUzFGOKccS2iJWJNYn1ir0RJ4hriDuJrxbPFi8WPyd+T/y1BEFCU8JFgiWxWaJM4oLEgMSUJFXSWNJfMlVyt2St5C3JcSmclKaUmxRHKkfquNRVqREqiqpGdaGyqTuoJ6jXqaM0LE2LxqQl0fJpp2ldtElpKWkz6TDpddJl0pekh+gouiadSU+hF9DP0vvpX5YpLnNaFrds17L6Zb3LpmXkZRxl4mTyZBpk+mS+yDJk3WSTZffJNss+lUPL6coFyq2VOyJ3Xe61PE3eVp4tnyd/Vv6RAqygqxCksEHhuMJdhSlFJUUPRb7iIcWriq+V6EqOSklKRUptShPKVGV7Za5ykfJl5ZcMaYYTI4VRwrjGmFRRUPFUEaocU+lSmVHVUg1V3a7aoPpUjahmpRavVqTWoTaprqzuq75RvU79kQZBw0ojUeOgRqfGtKaWZrjmTs1mzXEtGS2mVrZWndYTbYq2g3a6doX2Ax2sjpVOss5hnW5dWNdcN1G3TPeeHqxnocfVO6zXo4/Rt9bn6VfoDxiQDZwMsgzqDIYN6YY+htsNmw3fLFdfHrV83/LO5d+NzI1SjE4YPTaWMvYy3m7cavzORNeEbVJm8sCUYupuusW0xfStmZ5ZnNkRs0Fzqrmv+U7zDvNvFpYWAot6iwlLdcsYy3LLASuaVYDVbqub1hhrZ+st1hetP9tY2GTanLX509bANtm21nZ8hdaKuBUnVozYqdqx7I7ZDdkz7GPsf7YfclBxYDlUODx3VHPkOFY6jjnpOCU5nXJ642zkLHBudJ52sXHZ5NLuinL1cM1z7XKTcgt1K3V75q7qnuBe5z7pYe6xwaPdE+Pp7bnPc4CpyGQza5iTXpZem7yueZO9g71LvZ/76PoIfFp9YV8v3/2+T/w0/Hh+zf7An+m/3/9pgFZAesCvgdjAgMCywBdBxkEbgzqDqcFrgmuDP4Y4hxSEPA7VDhWGdoSJh0WH1YRNh7uGF4YPRSyP2BRxJ1IukhvZEoWLCouqjJpa6bbywMrRaPPo3Oj+VVqr1q26tVpudcrqS2vE17DWnIvBxITH1MZ8ZfmzKlhTsczY8thJtgv7IPsVx5FTxJmIs4srjBuLt4svjB9PsEvYnzCR6JBYnPia68It5b5N8kw6mjSd7J9clTybEp7SkIpPjUm9wJPiJfOupSmlrUvr4evxc/lD6TbpB9InBd6CygwoY1VGSyYNMTt3hdrCH4TDWfZZZVmf1oatPbdOch1v3d31uut3rR/Lds8+uQG9gb2hY6PKxm0bhzc5bTq2Gdocu7lji9qWnC2jWz22Vm8jbkve9tt2o+2F2z/sCN/RmqOYszVn5AePH+pyxXIFuQM7bXce/RH9I/fHrl2muw7t+p7Hybudb5RfnP91N3v37Z+Mfyr5aXZP/J6uAouCI3uxe3l7+/c57KsulCzMLhzZ77u/qYhRlFf04cCaA7eKzYqPHiQeFB4cKvEpaTmkfmjvoa+liaV9Zc5lDeUK5bvKpw9zDvcecTxSf1TxaP7RLz9zfx485nGsqUKzovg49njW8Rcnwk50nrQ6WVMpV5lf+a2KVzVUHVR9rcaypqZWobagDq4T1k2cij7Vfdr1dEu9Qf2xBnpD/hlwRnjm5S8xv/Sf9T7bcc7qXP15jfPljdTGvCaoaX3TZHNi81BLZEvPBa8LHa22rY2/Gv5adVHlYtkl6UsFbcS2nLbZy9mXp9r57a+vJFwZ6VjT8fhqxNUH1wKvdV33vn7zhvuNq51OnZdv2t28eMvm1oXbVreb71jcabprfrfxN/PfGrssupruWd5r6bbubu1Z0dPW69B75b7r/RsPmA/u9Pn19fSH9g8ORA8MDXIGxx+mPHz7KOvRzOOtTzBP8p5KPC1+pvCs4ned3xuGLIYuDbsO330e/PzxCHvk1R8Zf3wdzXlBeVE8pjxWM24yfnHCfaL75cqXo6/4r2Ze5/5N8m/lb7TfnP/T8c+7kxGTo28Fb2ff7X4v+77qg9mHjqmAqWcfUz/OTOd9kv1U/dnqc+eX8C9jM2u/4r6WfNP51vrd+/uT2dTZWT5LwJq3Aigk4Ph4AN5VAUCJBICK+Aqi2IJHnhe04OvnCfwnXvDR87IAoMYRgDmr5ovkw0jWRLI4EgFIhDgC2NRUFP9QRrypyUIvUjNiTYpnZ98j3hCnA8C3gdnZmebZ2W+VyLCPAGj/uODN5ySB+P9unpFpmE8vdyf4V/0dESMFV6aFJKkAAAGbaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjMxPC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjI3PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+ClDHTXEAAAIgSURBVEgN7Za5jsIwEIYnsAJEQUvBIXF1dCBEQcsTICHR8FA8By0vQEdPQckl7rvhhtVv4SiJ7Q272VWaHcmKY3vmyz++olUqlSe5ZB6XuAz7D3cl+x9G6uVyodvtRvf7nZ5PcR16vV5CCQQCRrcf103wx+NB+XyeCoWCMmCj0SCMg2maRh6Ph72jjbdbnfm4YDBo6hLgoVCIUFRWLpdpv9+zbgRdLpfU7/epVqt9CUcmW62WKawJbup5vQBgtGKxqE8J+jqdDoPW63XjMKE+m82+B282m3S9XnWYEPHVkMvllKq5T6/X41X9qVR+OBxot9tRMpmkWCymO1grSGepVLL9wOFwaHUlJXyz2dDxeKRqtUrpdFpwtDaoFhsfN5lMeFV/SuGYSyjHtoJqu8B6NEXlfD7TdDoVeqVwjJrP5wyMD3AKH41GhDPEalI4lCPt2WzWMRhApPwtOMBIE/ZyNBr9FTi2GXaN1QTlgOPgwOBIJOIYjniA48i2mgDHAGwxOMXjccdwpHswGLwHB3S1WlE4HGYf4GSxIRYWLrJoqxyDUcbjMaVSKceqcQAtFgvpfCPDpj8ZgHGlQnkikWBwKHdSsM1kqgE3zTmuR74yM5mMY+UQg/mWrXQpHCcRlHa7XXZXy34q4PiOQQy2LLIpM0H56XRi895ut8nv99teGLKgvA3w7XarjCHA4ejz+Wi9XvMYf/Y0Lbg/oygCuwr/BLz2S1Kr7o1KAAAAAElFTkSuQmCC",
      audio: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAZCAYAAAArK+5dAAAKqWlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUU2kWx7/30hstIdIJvSO9Sq+h9yYqIaGEEkIgqNiVwREcUUREQB1RqQqOSpFRRCzYBgGlWAdkEFDXwYKoqOwDlrCze3b37D/n5v7Ozffuu+/L+875A0AeZPH5KbAEAKm8TEGQhzMjIjKKgRsGEICBFNAFcix2Bt8pIMAHIFrMf9XHfmQ1ovsGc73+/ff/KklOXAYbACgA4VhOBjsV4fNItLH5gkwAUEgAtbWZ/DkuQZgmQAZE+NQcJyxw+xzHLvCD+TUhQS4IjwOAJ7NYggQASB+QOiOLnYD0IdMQNuJxuDyEXRG2ZyeyOAjnIKyfmpo2x2cQ1o79pz4Jf+kZK+rJYiWIeOFZ5oV35WbwU1jr/8/t+N9KTREu3kMVCXKiwDMIyXRkz6qT07xFzIv1819kLmd+/TwnCj1DF5md4RK1yByWq/ciC5NDnRaZJVi6lpvJDFlkQVqQqD8vxc9H1D+OKeK4DLfgRY7nujMXOTsxJHyRs7hhfouckRzsvbTGRVQXCINEM8cL3EXPmJqxNBubtXSvzMQQz6UZIkTzcOJc3UR1XqhoPT/TWdSTnxKwNH+Kh6iekRUsujYTecEWOYnlFbDUJ0C0P8AVuAEf5MMAocAEmANjJJCpMuPWzb3TwCWNv17ATUjMZDghpyaOweSxDfUZJkbGlgDMncGFv/j94PzZguj4pVpaNwCWNQhULdVYMQC0ILshrbZU0zgJgPgfAFxis4WCrIUaeu4LA4hAHNCALFACakAbGCDzWQBb4IhM7AX8QQiIBKsBGySCVCAAa8FGsA3kgnywFxwApeAoOA6qwWlwFjSDi+AKuAHugG7QBx6DITAKXoFJ8BHMQBCEgygQFZKFlCENSA8ygawge8gN8oGCoEgoBkqAeJAQ2gjtgPKhQqgUOgbVQL9AF6Ar0C2oB3oIDUMT0DvoC4yCyTANVoQ14eWwFewEe8Mh8Co4AU6Hs+EceA9cAlfAp+Am+Ap8B+6Dh+BX8BQKoEgoOkoFZYCyQrmg/FFRqHiUALUZlYcqRlWg6lGtqE7UfdQQ6jXqMxqLpqIZaAO0LdoTHYpmo9PRm9G70aXoanQT+hr6PnoYPYn+jqFgFDB6GBsMExOBScCsxeRiijGVmEbMdUwfZhTzEYvF0rFaWEusJzYSm4TdgN2NPYxtwLZje7Aj2CkcDieL08PZ4fxxLFwmLhd3CHcKdxnXixvFfcKT8Mp4E7w7PgrPw2/HF+Nr8W34XvwYfoYgQdAg2BD8CRzCekIB4QShlXCPMEqYIUoStYh2xBBiEnEbsYRYT7xOfEJ8TyKRVEnWpEASl7SVVEI6Q7pJGiZ9JkuRdcku5GiykLyHXEVuJz8kv6dQKJoUR0oUJZOyh1JDuUp5RvkkRhUzFGOKccS2iJWJNYn1ir0RJ4hriDuJrxbPFi8WPyd+T/y1BEFCU8JFgiWxWaJM4oLEgMSUJFXSWNJfMlVyt2St5C3JcSmclKaUmxRHKkfquNRVqREqiqpGdaGyqTuoJ6jXqaM0LE2LxqQl0fJpp2ldtElpKWkz6TDpddJl0pekh+gouiadSU+hF9DP0vvpX5YpLnNaFrds17L6Zb3LpmXkZRxl4mTyZBpk+mS+yDJk3WSTZffJNss+lUPL6coFyq2VOyJ3Xe61PE3eVp4tnyd/Vv6RAqygqxCksEHhuMJdhSlFJUUPRb7iIcWriq+V6EqOSklKRUptShPKVGV7Za5ykfJl5ZcMaYYTI4VRwrjGmFRRUPFUEaocU+lSmVHVUg1V3a7aoPpUjahmpRavVqTWoTaprqzuq75RvU79kQZBw0ojUeOgRqfGtKaWZrjmTs1mzXEtGS2mVrZWndYTbYq2g3a6doX2Ax2sjpVOss5hnW5dWNdcN1G3TPeeHqxnocfVO6zXo4/Rt9bn6VfoDxiQDZwMsgzqDIYN6YY+htsNmw3fLFdfHrV83/LO5d+NzI1SjE4YPTaWMvYy3m7cavzORNeEbVJm8sCUYupuusW0xfStmZ5ZnNkRs0Fzqrmv+U7zDvNvFpYWAot6iwlLdcsYy3LLASuaVYDVbqub1hhrZ+st1hetP9tY2GTanLX509bANtm21nZ8hdaKuBUnVozYqdqx7I7ZDdkz7GPsf7YfclBxYDlUODx3VHPkOFY6jjnpOCU5nXJ642zkLHBudJ52sXHZ5NLuinL1cM1z7XKTcgt1K3V75q7qnuBe5z7pYe6xwaPdE+Pp7bnPc4CpyGQza5iTXpZem7yueZO9g71LvZ/76PoIfFp9YV8v3/2+T/w0/Hh+zf7An+m/3/9pgFZAesCvgdjAgMCywBdBxkEbgzqDqcFrgmuDP4Y4hxSEPA7VDhWGdoSJh0WH1YRNh7uGF4YPRSyP2BRxJ1IukhvZEoWLCouqjJpa6bbywMrRaPPo3Oj+VVqr1q26tVpudcrqS2vE17DWnIvBxITH1MZ8ZfmzKlhTsczY8thJtgv7IPsVx5FTxJmIs4srjBuLt4svjB9PsEvYnzCR6JBYnPia68It5b5N8kw6mjSd7J9clTybEp7SkIpPjUm9wJPiJfOupSmlrUvr4evxc/lD6TbpB9InBd6CygwoY1VGSyYNMTt3hdrCH4TDWfZZZVmf1oatPbdOch1v3d31uut3rR/Lds8+uQG9gb2hY6PKxm0bhzc5bTq2Gdocu7lji9qWnC2jWz22Vm8jbkve9tt2o+2F2z/sCN/RmqOYszVn5AePH+pyxXIFuQM7bXce/RH9I/fHrl2muw7t+p7Hybudb5RfnP91N3v37Z+Mfyr5aXZP/J6uAouCI3uxe3l7+/c57KsulCzMLhzZ77u/qYhRlFf04cCaA7eKzYqPHiQeFB4cKvEpaTmkfmjvoa+liaV9Zc5lDeUK5bvKpw9zDvcecTxSf1TxaP7RLz9zfx485nGsqUKzovg49njW8Rcnwk50nrQ6WVMpV5lf+a2KVzVUHVR9rcaypqZWobagDq4T1k2cij7Vfdr1dEu9Qf2xBnpD/hlwRnjm5S8xv/Sf9T7bcc7qXP15jfPljdTGvCaoaX3TZHNi81BLZEvPBa8LHa22rY2/Gv5adVHlYtkl6UsFbcS2nLbZy9mXp9r57a+vJFwZ6VjT8fhqxNUH1wKvdV33vn7zhvuNq51OnZdv2t28eMvm1oXbVreb71jcabprfrfxN/PfGrssupruWd5r6bbubu1Z0dPW69B75b7r/RsPmA/u9Pn19fSH9g8ORA8MDXIGxx+mPHz7KOvRzOOtTzBP8p5KPC1+pvCs4ned3xuGLIYuDbsO330e/PzxCHvk1R8Zf3wdzXlBeVE8pjxWM24yfnHCfaL75cqXo6/4r2Ze5/5N8m/lb7TfnP/T8c+7kxGTo28Fb2ff7X4v+77qg9mHjqmAqWcfUz/OTOd9kv1U/dnqc+eX8C9jM2u/4r6WfNP51vrd+/uT2dTZWT5LwJq3Aigk4Ph4AN5VAUCJBICK+Aqi2IJHnhe04OvnCfwnXvDR87IAoMYRgDmr5ovkw0jWRLI4EgFIhDgC2NRUFP9QRrypyUIvUjNiTYpnZ98j3hCnA8C3gdnZmebZ2W+VyLCPAGj/uODN5ySB+P9unpFpmE8vdyf4V/0dESMFV6aFJKkAAAGbaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjI0PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjI1PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Clj9dW4AAAGwSURBVEgN7VK5igJBEH2zrGjkLXhmorlHJKJfIIgf4Tf5HYaGhh6BCEaCggd4IRoIgo5Ugc3i9Ez34pptwTDV3e/V61ddRqPRMPHB+PpgbS79L6DssKVFzWYT2WxWSdQFWASIGI1GdflKnFTA7/czsVarIRwOK4s4AaQCgUAAhmEgFoshnU478ZVnUgG32y1uTu9BYoVCAT6fT1nwFSAVME1TvIPH42GxfD6PUqn0yleupQK73Q6hUIjJl8sFyWSS81QqxcI0adVqFS6XSynwLUNst1sEg0E+mk6nyGQynM/nc+RyOc7j8Tjq9TpGoxHI8TP2+z3ogs+QOjidTohEIoxZLpei9+PxGF6vl/e73S4IV6lUUCwWQS2kjwbjZ0gdHA4Hgbler1itVkgkErjf73zjcrmM2+2GTqfDTsmxXUgdEHmz2QjObDYT+WQyQa/Xw/l8ZhGn4kSSCtDBer2mH8disXim3O/hcMgCYtMhkbaI8MfjUdAob7VaYv2bxNYBTcNfhK0APXS73ca7QrYtotvT9LwbFgeDwUD7AXXELQ76/b4OTxtjcaDN1AR+XOABrk2SXn7tqhwAAAAASUVORK5CYII=",
      instrument: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAaCAYAAABGiCfwAAAKqWlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUU2kWx7/30hstIdIJvSO9Sq+h9yYqIaGEEkIgqNiVwREcUUREQB1RqQqOSpFRRCzYBgGlWAdkEFDXwYKoqOwDlrCze3b37D/n5v7Ozffuu+/L+875A0AeZPH5KbAEAKm8TEGQhzMjIjKKgRsGEICBFNAFcix2Bt8pIMAHIFrMf9XHfmQ1ovsGc73+/ff/KklOXAYbACgA4VhOBjsV4fNItLH5gkwAUEgAtbWZ/DkuQZgmQAZE+NQcJyxw+xzHLvCD+TUhQS4IjwOAJ7NYggQASB+QOiOLnYD0IdMQNuJxuDyEXRG2ZyeyOAjnIKyfmpo2x2cQ1o79pz4Jf+kZK+rJYiWIeOFZ5oV35WbwU1jr/8/t+N9KTREu3kMVCXKiwDMIyXRkz6qT07xFzIv1819kLmd+/TwnCj1DF5md4RK1yByWq/ciC5NDnRaZJVi6lpvJDFlkQVqQqD8vxc9H1D+OKeK4DLfgRY7nujMXOTsxJHyRs7hhfouckRzsvbTGRVQXCINEM8cL3EXPmJqxNBubtXSvzMQQz6UZIkTzcOJc3UR1XqhoPT/TWdSTnxKwNH+Kh6iekRUsujYTecEWOYnlFbDUJ0C0P8AVuAEf5MMAocAEmANjJJCpMuPWzb3TwCWNv17ATUjMZDghpyaOweSxDfUZJkbGlgDMncGFv/j94PzZguj4pVpaNwCWNQhULdVYMQC0ILshrbZU0zgJgPgfAFxis4WCrIUaeu4LA4hAHNCALFACakAbGCDzWQBb4IhM7AX8QQiIBKsBGySCVCAAa8FGsA3kgnywFxwApeAoOA6qwWlwFjSDi+AKuAHugG7QBx6DITAKXoFJ8BHMQBCEgygQFZKFlCENSA8ygawge8gN8oGCoEgoBkqAeJAQ2gjtgPKhQqgUOgbVQL9AF6Ar0C2oB3oIDUMT0DvoC4yCyTANVoQ14eWwFewEe8Mh8Co4AU6Hs+EceA9cAlfAp+Am+Ap8B+6Dh+BX8BQKoEgoOkoFZYCyQrmg/FFRqHiUALUZlYcqRlWg6lGtqE7UfdQQ6jXqMxqLpqIZaAO0LdoTHYpmo9PRm9G70aXoanQT+hr6PnoYPYn+jqFgFDB6GBsMExOBScCsxeRiijGVmEbMdUwfZhTzEYvF0rFaWEusJzYSm4TdgN2NPYxtwLZje7Aj2CkcDieL08PZ4fxxLFwmLhd3CHcKdxnXixvFfcKT8Mp4E7w7PgrPw2/HF+Nr8W34XvwYfoYgQdAg2BD8CRzCekIB4QShlXCPMEqYIUoStYh2xBBiEnEbsYRYT7xOfEJ8TyKRVEnWpEASl7SVVEI6Q7pJGiZ9JkuRdcku5GiykLyHXEVuJz8kv6dQKJoUR0oUJZOyh1JDuUp5RvkkRhUzFGOKccS2iJWJNYn1ir0RJ4hriDuJrxbPFi8WPyd+T/y1BEFCU8JFgiWxWaJM4oLEgMSUJFXSWNJfMlVyt2St5C3JcSmclKaUmxRHKkfquNRVqREqiqpGdaGyqTuoJ6jXqaM0LE2LxqQl0fJpp2ldtElpKWkz6TDpddJl0pekh+gouiadSU+hF9DP0vvpX5YpLnNaFrds17L6Zb3LpmXkZRxl4mTyZBpk+mS+yDJk3WSTZffJNss+lUPL6coFyq2VOyJ3Xe61PE3eVp4tnyd/Vv6RAqygqxCksEHhuMJdhSlFJUUPRb7iIcWriq+V6EqOSklKRUptShPKVGV7Za5ykfJl5ZcMaYYTI4VRwrjGmFRRUPFUEaocU+lSmVHVUg1V3a7aoPpUjahmpRavVqTWoTaprqzuq75RvU79kQZBw0ojUeOgRqfGtKaWZrjmTs1mzXEtGS2mVrZWndYTbYq2g3a6doX2Ax2sjpVOss5hnW5dWNdcN1G3TPeeHqxnocfVO6zXo4/Rt9bn6VfoDxiQDZwMsgzqDIYN6YY+htsNmw3fLFdfHrV83/LO5d+NzI1SjE4YPTaWMvYy3m7cavzORNeEbVJm8sCUYupuusW0xfStmZ5ZnNkRs0Fzqrmv+U7zDvNvFpYWAot6iwlLdcsYy3LLASuaVYDVbqub1hhrZ+st1hetP9tY2GTanLX509bANtm21nZ8hdaKuBUnVozYqdqx7I7ZDdkz7GPsf7YfclBxYDlUODx3VHPkOFY6jjnpOCU5nXJ642zkLHBudJ52sXHZ5NLuinL1cM1z7XKTcgt1K3V75q7qnuBe5z7pYe6xwaPdE+Pp7bnPc4CpyGQza5iTXpZem7yueZO9g71LvZ/76PoIfFp9YV8v3/2+T/w0/Hh+zf7An+m/3/9pgFZAesCvgdjAgMCywBdBxkEbgzqDqcFrgmuDP4Y4hxSEPA7VDhWGdoSJh0WH1YRNh7uGF4YPRSyP2BRxJ1IukhvZEoWLCouqjJpa6bbywMrRaPPo3Oj+VVqr1q26tVpudcrqS2vE17DWnIvBxITH1MZ8ZfmzKlhTsczY8thJtgv7IPsVx5FTxJmIs4srjBuLt4svjB9PsEvYnzCR6JBYnPia68It5b5N8kw6mjSd7J9clTybEp7SkIpPjUm9wJPiJfOupSmlrUvr4evxc/lD6TbpB9InBd6CygwoY1VGSyYNMTt3hdrCH4TDWfZZZVmf1oatPbdOch1v3d31uut3rR/Lds8+uQG9gb2hY6PKxm0bhzc5bTq2Gdocu7lji9qWnC2jWz22Vm8jbkve9tt2o+2F2z/sCN/RmqOYszVn5AePH+pyxXIFuQM7bXce/RH9I/fHrl2muw7t+p7Hybudb5RfnP91N3v37Z+Mfyr5aXZP/J6uAouCI3uxe3l7+/c57KsulCzMLhzZ77u/qYhRlFf04cCaA7eKzYqPHiQeFB4cKvEpaTmkfmjvoa+liaV9Zc5lDeUK5bvKpw9zDvcecTxSf1TxaP7RLz9zfx485nGsqUKzovg49njW8Rcnwk50nrQ6WVMpV5lf+a2KVzVUHVR9rcaypqZWobagDq4T1k2cij7Vfdr1dEu9Qf2xBnpD/hlwRnjm5S8xv/Sf9T7bcc7qXP15jfPljdTGvCaoaX3TZHNi81BLZEvPBa8LHa22rY2/Gv5adVHlYtkl6UsFbcS2nLbZy9mXp9r57a+vJFwZ6VjT8fhqxNUH1wKvdV33vn7zhvuNq51OnZdv2t28eMvm1oXbVreb71jcabprfrfxN/PfGrssupruWd5r6bbubu1Z0dPW69B75b7r/RsPmA/u9Pn19fSH9g8ORA8MDXIGxx+mPHz7KOvRzOOtTzBP8p5KPC1+pvCs4ned3xuGLIYuDbsO330e/PzxCHvk1R8Zf3wdzXlBeVE8pjxWM24yfnHCfaL75cqXo6/4r2Ze5/5N8m/lb7TfnP/T8c+7kxGTo28Fb2ff7X4v+77qg9mHjqmAqWcfUz/OTOd9kv1U/dnqc+eX8C9jM2u/4r6WfNP51vrd+/uT2dTZWT5LwJq3Aigk4Ph4AN5VAUCJBICK+Aqi2IJHnhe04OvnCfwnXvDR87IAoMYRgDmr5ovkw0jWRLI4EgFIhDgC2NRUFP9QRrypyUIvUjNiTYpnZ98j3hCnA8C3gdnZmebZ2W+VyLCPAGj/uODN5ySB+P9unpFpmE8vdyf4V/0dESMFV6aFJKkAAAGbaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjI3PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjI2PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+ChT7jXoAAACoSURBVEgNYwwODv7PQCfARCd7wNaMWkaV0GbBZkp6ejpceObMmXA2iGFiYsJgbGzMgEscpObs2bMMZ86cATFRwGicoQQHuZzRYCQ35FD0Dd9gZNbS0mpA8SuQA8q4MABiMzIyMjx79owBlNmlpKTAUrjEQZLPnz8Hq4eZAaOHbzCO+gwWxxTRdA1GrPUZqD5CBqBkDwKkiiObAWIzjrau0IOEHD5dEwgALDQwHLmf+zgAAAAASUVORK5CYII=",
      master: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAYCAYAAAAPtVbGAAAKqWlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUU2kWx7/30hstIdIJvSO9Sq+h9yYqIaGEEkIgqNiVwREcUUREQB1RqQqOSpFRRCzYBgGlWAdkEFDXwYKoqOwDlrCze3b37D/n5v7Ozffuu+/L+875A0AeZPH5KbAEAKm8TEGQhzMjIjKKgRsGEICBFNAFcix2Bt8pIMAHIFrMf9XHfmQ1ovsGc73+/ff/KklOXAYbACgA4VhOBjsV4fNItLH5gkwAUEgAtbWZ/DkuQZgmQAZE+NQcJyxw+xzHLvCD+TUhQS4IjwOAJ7NYggQASB+QOiOLnYD0IdMQNuJxuDyEXRG2ZyeyOAjnIKyfmpo2x2cQ1o79pz4Jf+kZK+rJYiWIeOFZ5oV35WbwU1jr/8/t+N9KTREu3kMVCXKiwDMIyXRkz6qT07xFzIv1819kLmd+/TwnCj1DF5md4RK1yByWq/ciC5NDnRaZJVi6lpvJDFlkQVqQqD8vxc9H1D+OKeK4DLfgRY7nujMXOTsxJHyRs7hhfouckRzsvbTGRVQXCINEM8cL3EXPmJqxNBubtXSvzMQQz6UZIkTzcOJc3UR1XqhoPT/TWdSTnxKwNH+Kh6iekRUsujYTecEWOYnlFbDUJ0C0P8AVuAEf5MMAocAEmANjJJCpMuPWzb3TwCWNv17ATUjMZDghpyaOweSxDfUZJkbGlgDMncGFv/j94PzZguj4pVpaNwCWNQhULdVYMQC0ILshrbZU0zgJgPgfAFxis4WCrIUaeu4LA4hAHNCALFACakAbGCDzWQBb4IhM7AX8QQiIBKsBGySCVCAAa8FGsA3kgnywFxwApeAoOA6qwWlwFjSDi+AKuAHugG7QBx6DITAKXoFJ8BHMQBCEgygQFZKFlCENSA8ygawge8gN8oGCoEgoBkqAeJAQ2gjtgPKhQqgUOgbVQL9AF6Ar0C2oB3oIDUMT0DvoC4yCyTANVoQ14eWwFewEe8Mh8Co4AU6Hs+EceA9cAlfAp+Am+Ap8B+6Dh+BX8BQKoEgoOkoFZYCyQrmg/FFRqHiUALUZlYcqRlWg6lGtqE7UfdQQ6jXqMxqLpqIZaAO0LdoTHYpmo9PRm9G70aXoanQT+hr6PnoYPYn+jqFgFDB6GBsMExOBScCsxeRiijGVmEbMdUwfZhTzEYvF0rFaWEusJzYSm4TdgN2NPYxtwLZje7Aj2CkcDieL08PZ4fxxLFwmLhd3CHcKdxnXixvFfcKT8Mp4E7w7PgrPw2/HF+Nr8W34XvwYfoYgQdAg2BD8CRzCekIB4QShlXCPMEqYIUoStYh2xBBiEnEbsYRYT7xOfEJ8TyKRVEnWpEASl7SVVEI6Q7pJGiZ9JkuRdcku5GiykLyHXEVuJz8kv6dQKJoUR0oUJZOyh1JDuUp5RvkkRhUzFGOKccS2iJWJNYn1ir0RJ4hriDuJrxbPFi8WPyd+T/y1BEFCU8JFgiWxWaJM4oLEgMSUJFXSWNJfMlVyt2St5C3JcSmclKaUmxRHKkfquNRVqREqiqpGdaGyqTuoJ6jXqaM0LE2LxqQl0fJpp2ldtElpKWkz6TDpddJl0pekh+gouiadSU+hF9DP0vvpX5YpLnNaFrds17L6Zb3LpmXkZRxl4mTyZBpk+mS+yDJk3WSTZffJNss+lUPL6coFyq2VOyJ3Xe61PE3eVp4tnyd/Vv6RAqygqxCksEHhuMJdhSlFJUUPRb7iIcWriq+V6EqOSklKRUptShPKVGV7Za5ykfJl5ZcMaYYTI4VRwrjGmFRRUPFUEaocU+lSmVHVUg1V3a7aoPpUjahmpRavVqTWoTaprqzuq75RvU79kQZBw0ojUeOgRqfGtKaWZrjmTs1mzXEtGS2mVrZWndYTbYq2g3a6doX2Ax2sjpVOss5hnW5dWNdcN1G3TPeeHqxnocfVO6zXo4/Rt9bn6VfoDxiQDZwMsgzqDIYN6YY+htsNmw3fLFdfHrV83/LO5d+NzI1SjE4YPTaWMvYy3m7cavzORNeEbVJm8sCUYupuusW0xfStmZ5ZnNkRs0Fzqrmv+U7zDvNvFpYWAot6iwlLdcsYy3LLASuaVYDVbqub1hhrZ+st1hetP9tY2GTanLX509bANtm21nZ8hdaKuBUnVozYqdqx7I7ZDdkz7GPsf7YfclBxYDlUODx3VHPkOFY6jjnpOCU5nXJ642zkLHBudJ52sXHZ5NLuinL1cM1z7XKTcgt1K3V75q7qnuBe5z7pYe6xwaPdE+Pp7bnPc4CpyGQza5iTXpZem7yueZO9g71LvZ/76PoIfFp9YV8v3/2+T/w0/Hh+zf7An+m/3/9pgFZAesCvgdjAgMCywBdBxkEbgzqDqcFrgmuDP4Y4hxSEPA7VDhWGdoSJh0WH1YRNh7uGF4YPRSyP2BRxJ1IukhvZEoWLCouqjJpa6bbywMrRaPPo3Oj+VVqr1q26tVpudcrqS2vE17DWnIvBxITH1MZ8ZfmzKlhTsczY8thJtgv7IPsVx5FTxJmIs4srjBuLt4svjB9PsEvYnzCR6JBYnPia68It5b5N8kw6mjSd7J9clTybEp7SkIpPjUm9wJPiJfOupSmlrUvr4evxc/lD6TbpB9InBd6CygwoY1VGSyYNMTt3hdrCH4TDWfZZZVmf1oatPbdOch1v3d31uut3rR/Lds8+uQG9gb2hY6PKxm0bhzc5bTq2Gdocu7lji9qWnC2jWz22Vm8jbkve9tt2o+2F2z/sCN/RmqOYszVn5AePH+pyxXIFuQM7bXce/RH9I/fHrl2muw7t+p7Hybudb5RfnP91N3v37Z+Mfyr5aXZP/J6uAouCI3uxe3l7+/c57KsulCzMLhzZ77u/qYhRlFf04cCaA7eKzYqPHiQeFB4cKvEpaTmkfmjvoa+liaV9Zc5lDeUK5bvKpw9zDvcecTxSf1TxaP7RLz9zfx485nGsqUKzovg49njW8Rcnwk50nrQ6WVMpV5lf+a2KVzVUHVR9rcaypqZWobagDq4T1k2cij7Vfdr1dEu9Qf2xBnpD/hlwRnjm5S8xv/Sf9T7bcc7qXP15jfPljdTGvCaoaX3TZHNi81BLZEvPBa8LHa22rY2/Gv5adVHlYtkl6UsFbcS2nLbZy9mXp9r57a+vJFwZ6VjT8fhqxNUH1wKvdV33vn7zhvuNq51OnZdv2t28eMvm1oXbVreb71jcabprfrfxN/PfGrssupruWd5r6bbubu1Z0dPW69B75b7r/RsPmA/u9Pn19fSH9g8ORA8MDXIGxx+mPHz7KOvRzOOtTzBP8p5KPC1+pvCs4ned3xuGLIYuDbsO330e/PzxCHvk1R8Zf3wdzXlBeVE8pjxWM24yfnHCfaL75cqXo6/4r2Ze5/5N8m/lb7TfnP/T8c+7kxGTo28Fb2ff7X4v+77qg9mHjqmAqWcfUz/OTOd9kv1U/dnqc+eX8C9jM2u/4r6WfNP51vrd+/uT2dTZWT5LwJq3Aigk4Ph4AN5VAUCJBICK+Aqi2IJHnhe04OvnCfwnXvDR87IAoMYRgDmr5ovkw0jWRLI4EgFIhDgC2NRUFP9QRrypyUIvUjNiTYpnZ98j3hCnA8C3gdnZmebZ2W+VyLCPAGj/uODN5ySB+P9unpFpmE8vdyf4V/0dESMFV6aFJKkAAAGbaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjI1PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjI0PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CmMA3WIAAAGXSURBVEgN5ZM/rwFREMXPPk+2EAWJKEQhEdGI+Bcq0Wm2U/tgvpREJQqCQhBBNChkX85NVvbaWdYmGm+a2TtzZn57Z2eNXq9n48P28+H+qv0/hcTjcTSbzbcn/Na4Go0GyuUyksnkW6DAkFQqhXw+D8Mw0G63lQ9K0iCxWMy3jmMigJZOp5HL5UQtNaZpajkN0mq1YFkWstnsvSHVxWIRmUxGK6Q2Go3eY2zMUfb7ffDbue3XfZhOp+h2u6rh4XDAaDTCcrlEvV53y9QzG5VKJYzHY1QqFRQKBXWD4/GI3W6n6TXIZrOBbdvqFolEAp1OB7fbDZFIRCtyDoRXq1Utv16vnfTda+M6n8/gm7jND0AN5/+Yfwlh4Ww2owtlnMJ8PvfUajdhliMLa/v9Htfr1VPugWy3W/VdPMoAgdVqJao8EL7JYrEQxa+C3E7JtO1yBMPhUJytk/fzj6vr6ETI6XTC5XJxNIE9110yEcKfrFarSfqnscFgIOZFCK89mUzEgjBBEcJdl/Y9DIA1nu0K2+hZ3fdA/gC/V3gXMB9RLgAAAABJRU5ErkJggg==",
      send: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAXCAYAAAAP6L+eAAAKqWlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUU2kWx7/30hstIdIJvSO9Sq+h9yYqIaGEEkIgqNiVwREcUUREQB1RqQqOSpFRRCzYBgGlWAdkEFDXwYKoqOwDlrCze3b37D/n5v7Ozffuu+/L+875A0AeZPH5KbAEAKm8TEGQhzMjIjKKgRsGEICBFNAFcix2Bt8pIMAHIFrMf9XHfmQ1ovsGc73+/ff/KklOXAYbACgA4VhOBjsV4fNItLH5gkwAUEgAtbWZ/DkuQZgmQAZE+NQcJyxw+xzHLvCD+TUhQS4IjwOAJ7NYggQASB+QOiOLnYD0IdMQNuJxuDyEXRG2ZyeyOAjnIKyfmpo2x2cQ1o79pz4Jf+kZK+rJYiWIeOFZ5oV35WbwU1jr/8/t+N9KTREu3kMVCXKiwDMIyXRkz6qT07xFzIv1819kLmd+/TwnCj1DF5md4RK1yByWq/ciC5NDnRaZJVi6lpvJDFlkQVqQqD8vxc9H1D+OKeK4DLfgRY7nujMXOTsxJHyRs7hhfouckRzsvbTGRVQXCINEM8cL3EXPmJqxNBubtXSvzMQQz6UZIkTzcOJc3UR1XqhoPT/TWdSTnxKwNH+Kh6iekRUsujYTecEWOYnlFbDUJ0C0P8AVuAEf5MMAocAEmANjJJCpMuPWzb3TwCWNv17ATUjMZDghpyaOweSxDfUZJkbGlgDMncGFv/j94PzZguj4pVpaNwCWNQhULdVYMQC0ILshrbZU0zgJgPgfAFxis4WCrIUaeu4LA4hAHNCALFACakAbGCDzWQBb4IhM7AX8QQiIBKsBGySCVCAAa8FGsA3kgnywFxwApeAoOA6qwWlwFjSDi+AKuAHugG7QBx6DITAKXoFJ8BHMQBCEgygQFZKFlCENSA8ygawge8gN8oGCoEgoBkqAeJAQ2gjtgPKhQqgUOgbVQL9AF6Ar0C2oB3oIDUMT0DvoC4yCyTANVoQ14eWwFewEe8Mh8Co4AU6Hs+EceA9cAlfAp+Am+Ap8B+6Dh+BX8BQKoEgoOkoFZYCyQrmg/FFRqHiUALUZlYcqRlWg6lGtqE7UfdQQ6jXqMxqLpqIZaAO0LdoTHYpmo9PRm9G70aXoanQT+hr6PnoYPYn+jqFgFDB6GBsMExOBScCsxeRiijGVmEbMdUwfZhTzEYvF0rFaWEusJzYSm4TdgN2NPYxtwLZje7Aj2CkcDieL08PZ4fxxLFwmLhd3CHcKdxnXixvFfcKT8Mp4E7w7PgrPw2/HF+Nr8W34XvwYfoYgQdAg2BD8CRzCekIB4QShlXCPMEqYIUoStYh2xBBiEnEbsYRYT7xOfEJ8TyKRVEnWpEASl7SVVEI6Q7pJGiZ9JkuRdcku5GiykLyHXEVuJz8kv6dQKJoUR0oUJZOyh1JDuUp5RvkkRhUzFGOKccS2iJWJNYn1ir0RJ4hriDuJrxbPFi8WPyd+T/y1BEFCU8JFgiWxWaJM4oLEgMSUJFXSWNJfMlVyt2St5C3JcSmclKaUmxRHKkfquNRVqREqiqpGdaGyqTuoJ6jXqaM0LE2LxqQl0fJpp2ldtElpKWkz6TDpddJl0pekh+gouiadSU+hF9DP0vvpX5YpLnNaFrds17L6Zb3LpmXkZRxl4mTyZBpk+mS+yDJk3WSTZffJNss+lUPL6coFyq2VOyJ3Xe61PE3eVp4tnyd/Vv6RAqygqxCksEHhuMJdhSlFJUUPRb7iIcWriq+V6EqOSklKRUptShPKVGV7Za5ykfJl5ZcMaYYTI4VRwrjGmFRRUPFUEaocU+lSmVHVUg1V3a7aoPpUjahmpRavVqTWoTaprqzuq75RvU79kQZBw0ojUeOgRqfGtKaWZrjmTs1mzXEtGS2mVrZWndYTbYq2g3a6doX2Ax2sjpVOss5hnW5dWNdcN1G3TPeeHqxnocfVO6zXo4/Rt9bn6VfoDxiQDZwMsgzqDIYN6YY+htsNmw3fLFdfHrV83/LO5d+NzI1SjE4YPTaWMvYy3m7cavzORNeEbVJm8sCUYupuusW0xfStmZ5ZnNkRs0Fzqrmv+U7zDvNvFpYWAot6iwlLdcsYy3LLASuaVYDVbqub1hhrZ+st1hetP9tY2GTanLX509bANtm21nZ8hdaKuBUnVozYqdqx7I7ZDdkz7GPsf7YfclBxYDlUODx3VHPkOFY6jjnpOCU5nXJ642zkLHBudJ52sXHZ5NLuinL1cM1z7XKTcgt1K3V75q7qnuBe5z7pYe6xwaPdE+Pp7bnPc4CpyGQza5iTXpZem7yueZO9g71LvZ/76PoIfFp9YV8v3/2+T/w0/Hh+zf7An+m/3/9pgFZAesCvgdjAgMCywBdBxkEbgzqDqcFrgmuDP4Y4hxSEPA7VDhWGdoSJh0WH1YRNh7uGF4YPRSyP2BRxJ1IukhvZEoWLCouqjJpa6bbywMrRaPPo3Oj+VVqr1q26tVpudcrqS2vE17DWnIvBxITH1MZ8ZfmzKlhTsczY8thJtgv7IPsVx5FTxJmIs4srjBuLt4svjB9PsEvYnzCR6JBYnPia68It5b5N8kw6mjSd7J9clTybEp7SkIpPjUm9wJPiJfOupSmlrUvr4evxc/lD6TbpB9InBd6CygwoY1VGSyYNMTt3hdrCH4TDWfZZZVmf1oatPbdOch1v3d31uut3rR/Lds8+uQG9gb2hY6PKxm0bhzc5bTq2Gdocu7lji9qWnC2jWz22Vm8jbkve9tt2o+2F2z/sCN/RmqOYszVn5AePH+pyxXIFuQM7bXce/RH9I/fHrl2muw7t+p7Hybudb5RfnP91N3v37Z+Mfyr5aXZP/J6uAouCI3uxe3l7+/c57KsulCzMLhzZ77u/qYhRlFf04cCaA7eKzYqPHiQeFB4cKvEpaTmkfmjvoa+liaV9Zc5lDeUK5bvKpw9zDvcecTxSf1TxaP7RLz9zfx485nGsqUKzovg49njW8Rcnwk50nrQ6WVMpV5lf+a2KVzVUHVR9rcaypqZWobagDq4T1k2cij7Vfdr1dEu9Qf2xBnpD/hlwRnjm5S8xv/Sf9T7bcc7qXP15jfPljdTGvCaoaX3TZHNi81BLZEvPBa8LHa22rY2/Gv5adVHlYtkl6UsFbcS2nLbZy9mXp9r57a+vJFwZ6VjT8fhqxNUH1wKvdV33vn7zhvuNq51OnZdv2t28eMvm1oXbVreb71jcabprfrfxN/PfGrssupruWd5r6bbubu1Z0dPW69B75b7r/RsPmA/u9Pn19fSH9g8ORA8MDXIGxx+mPHz7KOvRzOOtTzBP8p5KPC1+pvCs4ned3xuGLIYuDbsO330e/PzxCHvk1R8Zf3wdzXlBeVE8pjxWM24yfnHCfaL75cqXo6/4r2Ze5/5N8m/lb7TfnP/T8c+7kxGTo28Fb2ff7X4v+77qg9mHjqmAqWcfUz/OTOd9kv1U/dnqc+eX8C9jM2u/4r6WfNP51vrd+/uT2dTZWT5LwJq3Aigk4Ph4AN5VAUCJBICK+Aqi2IJHnhe04OvnCfwnXvDR87IAoMYRgDmr5ovkw0jWRLI4EgFIhDgC2NRUFP9QRrypyUIvUjNiTYpnZ98j3hCnA8C3gdnZmebZ2W+VyLCPAGj/uODN5ySB+P9unpFpmE8vdyf4V/0dESMFV6aFJKkAAAGbaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjIyPC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjIzPC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CsDwhUYAAAErSURBVDgR7ZQ9ioRAEIXf7CyIgomgGIogiGBo6AnEQLzQXEm8hoJH8ABmIqggu1PLKDq2bU9gtgVN/736Cp9F39I0/cEF8XUB8w/5D16cvcyK76XEa+H7Pmjwous6ZFnGk2AHliQJqqpyk0Qud+C6rtG2LTPXNE24rsu8ez/cgZumAY2jEAVf9vNOwSJ+szRccBAEeL4l0HX9yBmEYYgkSXY//BBM3eE4DmiOoogJp8Ke50GWZViWtSl+f148NievzTRNoA6hBEVRYNs2+r6HYRgYxxGapi39XpYlqqraYG5nzyb5F8fx7lNnCkGLopi3y3xoxaygns7znNnbR1DKPQWTiAXnQYXB7/AzKOlPPSbROqhLhmFYHzHXQlasM0WgpP8YvC7CW18G/gWUB2SrMTPVTwAAAABJRU5ErkJggg=="
    }
    const classes = classnames(
      style.view,
      style[`-${type.toLowerCase()}`],
      selected ? style['-selected'] : null,
      notSelected ? style['-not-selected'] : null,
      eqing ? style['-eqing'] : null
    )
    return (
      <div data-name={taglessName} data-tag={inTag} data-index={track.index} onClick={this.selectTrack.bind(this)} className={classes}>

        <div className={style.bgImage} style={{ backgroundImage: `url(${this.getImage()})` }}></div>
        <div className={style.trackColor} style={{ background: color }}></div>
        {/* <EQComponent track={track} eqing={this.props.eqing}></EQComponent> */}
        <div className={style.content}>
          <div className={style.trackName}>
            <img src={images[type.toLowerCase()]} />
            <div
              onKeyDown={this.onKeyDownName.bind(this)}
              onKeyUp={this.onKeyUpName.bind(this)}
              ref={node => this.nameElement = node}
              contentEditable={this.state.editingName}
              className={classnames(style.renamer, this.state.editingName ? style['-editing'] : null)}>
              {getTagglessName(track)}
            </div>
          </div>

          <div className={style.footer}>
            <div className={style.volume}>{volumeString}</div>
            <div className={style.buttons}>
              <div onClick={this.onMute.bind(this)} className={classnames(style.button, style.mute, { [style.muted]: mute })}>M</div>
              <div onClick={this.onSolo.bind(this)} className={classnames(style.button, style.solo, { [style.soloed]: solo })}>S</div>
              <div onClick={this.onVolumeDown.bind(this)} className={classnames(style.button, style.minus)}>-</div>
              <div onClick={this.onVolumeUp.bind(this)} className={classnames(style.button, style.plus)}>+</div>
              <div onClick={this.toggleOptions.bind(this)}><i className="fa fa-ellipsis-h"></i></div>
            </div>
          </div>
        </div>
        <div className={style.levelMeter} ref={ref => this.levelMeter = ref}></div>
        {this.renderOptions()}
      </div>
    );
  }
}

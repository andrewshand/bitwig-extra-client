import { Bitwig } from './Bitwig'
import * as _ from 'underscore'
export const TAG_CHAR = '#'
export const TAG_REGEX = new RegExp(`${TAG_CHAR}([^ ]+)`, 'g')

export function getTrackTags(track: any): string[] {
    const matches = track.name.match(TAG_REGEX)
    if (!matches) {
        return []
    }
    const withSymbolRemoved = matches.map(function (tag) {
        return tag.substr(1)
    })
    return _.uniq(withSymbolRemoved)
}
export function getTagglessName(track: Bitwig.TrackPacket) {
    return track.name.replace(TAG_REGEX, '').trim()
}

export function isUnusableTag(tag: string) {
    return tag.toLowerCase() === 'all' || tag.toLowerCase() === 'untagged'
}

export function isAvailableTag(tag: string) {
    return !isUnusableTag(tag)
}
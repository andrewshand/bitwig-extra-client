# Bitwig Extra Client

![alt text](http://i.imgur.com/IIntWDP.png)

The frontend for Bitwig Extra, written in typescript and react. Started from the react typescript boilerplate, still lots of irrelevant code remaining (that needs removing). 

You need to also have built https://github.com/andrewshand/bitwig-extra-server and be running it as an extension in Bitwig.

If you want to work on this, the majority of the code is in src/components

Better instructions soon! Help appreciated

## Setup

```
$ npm install
```

## Running

```
$ npm start
```

## Build

```
$ npm run build
``
